<%@page import="net.vinace.app.product.ProductVO"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>产品信息</title>
</head>
<body>

	<div
		class="modal fade"
		id="productDialog"
		tabindex="-1"
		role="dialog"
		aria-labelledby="productDialogLabel"
		aria-hidden="true">
		<form id="productInfo" method="post" action="save.do">
		<div
			class="modal-dialog"
			tabindex="0">
			<div class="modal-content">
				<div class="modal-header">
					<button
						type="button"
						class="close"
						data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4
						class="modal-title"
						id="myModalLabel">产品信息<input id="pid" name="pid" type="text"/> </h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered">
						<tr>
							<td style="text-align: right;">产品编号:</td>
							<td><input
								style="width: 200px;"
								type="text" id="productNo" name="productNo" value="${vo.productNo}" /></td>
						</tr>
						<td style="text-align: right;">产品名称:</td>
						<td><input
							style="width: 200px;"
							type="text" id="productName"  name="productName" /></td>
						</tr>
						<tr>
							<td style="text-align: right; ">价格:</td>
							<td><input
								style="width: 200px;"
								type="text" id="price" name="price"/></td>
						</tr>
						<tr>
							<td style="text-align: right;">单位:</td>
							<td><input
								style="width: 200px;"
								type="text" id="rule" name="rule"/></td>
						</tr>
						<tr>
							<td style="text-align: right;">备注:</td>
							<td><textarea style="width: 200px;"  name="note" id="note"></textarea></td>
						</tr>
					</table>
				</div>
				<div id="optBtn" class="modal-footer">
					
					<button id="saveBtn"
						type="button"
						class="btn btn-primary">保存</button>
						<button
						type="button"
						class="btn btn-default"
						data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</body>
</html>