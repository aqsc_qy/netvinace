<%@ page
	language="java"
	contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib
	uri="http://java.sun.com/jsp/jstl/core"
	prefix="c"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta
	http-equiv="X-UA-Compatible"
	content="IE=edge">

<meta
	name="viewport"
	content="width=device-width, initial-scale=1">
<title>demo</title>
<jsp:include page="addProduct.jsp"></jsp:include>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../manager/product/js/jquery-1.9.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../manager/product/js/bootstrap.min.js"></script>
<script src="../manager/product/js/jqPaginator.js"></script>
<script  src="../manager/product/js/product.js"></script>
<!-- Bootstrap -->

<link
	href="../manager/product/css/bootstrap.min.css"
	rel="stylesheet">


<style>
body {
	font-size: 16px;
	font-family: Arial, Helvetica, sans-serif;
}
</style>



</head>
<body>
	<div
		class="container-fluid"
		style="width: 960px;">
		<div class="row-fluid">
			<h3>产品列表</h3>
		</div>

		<div class="row">
			<div class="col-md-2 pull-right">
				<ul
					class="nav nav-pills nav-justified"
					role="tablist">
					<li
						role="presentation"
						class=" active"><a href="#">Home</a></li>
					<li role="presentation"><a href="#">Profile</a></li>
					<li role="presentation"><a href="#">Messages</a></li>

				</ul>
			</div>
		</div>

		<div
			class="row"
			style="margin-top: 30px;">
			<div
				class="col-sm-2"
				style="border-width: 2px; border-color: #AA0000">
				<ul
					class="nav nav-pills nav-stacked"
					role="tablist">
					<li
						role="presentation"
						class="active"><a href="#">Home</a></li>
					<li role="presentation"><a href="#">Profile</a></li>
					<li role="presentation"><a href="#">Messages</a></li>
				</ul>
			</div>
			<div
				class=" col-sm-8"
				style="margin-left: 50px; margin-right: 0; border-width: 2px; border-color: #AA0000">

				<div id="tools" class="col-md-4 pull-right">
					<ul
						class="nav nav-pills nav-justified "
						role="tablist">
						<li
							role="presentation"
							class=" active"
							style="padding-left: 10px;">
							<button class="btn  btn-sm btn-info">查询</button>
						</li>
						<li
							role="presentation"
							class=" active"
							style="padding-left: 10px;">
							<button
								id="addProduct"
								class="btn btn-sm btn-success"
								data-toggle="modal">添加产品</button>
						</li>
						<li
							role="presentation"
							class=" active"
							style="padding-left: 10px;">
							<button
								id="deleteBtn"
								class="btn btn-sm btn-danger">删除</button>
						</li>
					</ul>
				</div>
				<div style="margin-top: 40px;">

					<table
						id="productList"
						class="table table-bordered table-hover">
						<tr>
							<td><input
								id="selectAll"
								class="checkbox"
								type="checkbox" /></td>
							<td>产品编号</td>
							<td>产品名称</td>
							<td>产品规格</td>
							<td>产品数量</td>
							<td>产品价格</td>
							<td>操作</td>
						</tr>

						<c:forEach
							var="product"
							items="${pageResult.resultList}">
							<tr>
								<td><input
									name="proId"
									class="checkbox"
									type="checkbox"
									value="${product.pid}" /></td>
								<td>${product.product_code}</td>
								<td>${product.product_name}</td>
								<td>${product.detail}</td>
								<td>${product.note}</td>
								<td>${product.product_code}</td>
								<td><button
										name="editBtn"
										objId="${product.pid}"
										class="btn btn-sm-block"
										onclick="">修改</button></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="p2"></div>
	<ul class="pagination" id="pagination2"></ul>
	<div style="text-align: center;">
<div id="pageResult">
<input id="pageCount" type="hidden" value="${pageResult.pageCount}"/>
<input id="totalCount" type="hidden" value="${pageResult.totalCount}"/>
<input id="pageIndex" type="hidden" value="${pageResult.pageIndex}"/>
</div>
		<!-- footer -->

		<p>vinace-Demo by 御风林海</p>

	</div>

</body>

</html>