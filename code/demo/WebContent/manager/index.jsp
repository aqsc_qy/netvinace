<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!doctype html>
<html>
<head>

  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
  <meta charset="utf-8" />

  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />

  <!-- Use title if it's in the page YAML frontmatter -->
  <title>Core Admin Theme</title>

  <!--[if lt IE 9]>
  <script src="javascripts/vendor/html5shiv.js" type="text/javascript"></script>
  <script src="javascripts/vendor/excanvas.js" type="text/javascript"></script>
  <![endif]-->

  <script src="manager/javascripts/application.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>



<body>
<jsp:include page="/manager/menuInfor.jsp"></jsp:include>

</body>
</html>