$(document)
		.ready(
				function() {
					var pSize = 10;
					var totalCount = $("#totalCount").val();
					var pageIndex = $("#pageIndex").val();
					// alert(pageCount);
					/**
					 * 全选当前页记录
					 */
					$("#productList").delegate('#selectAll', 'click',
							function() {
								var isSelectAll = $(this).prop('checked');
								$("input[name='proId']").each(function() {
									$(this).prop("checked", isSelectAll);
								});
							});

					/**
					 * 修改按钮事件
					 */

					$("#productList").delegate('button', 'click', function() {
						var proId = $(this).attr("objId");

						var editUrl = "show.do";
						$.ajax({

							url : editUrl,
							dataType : 'json',
							data : {
								id : proId
							},
							cache : false,

							success : function(data) {
								alert("--------------" + data.pid);
								loadProduct(data);
							},
							error : function(html) {
								// alert(html);
								alert("Load Data failly");
								refresh();
							}
						});

					});

					$("#tools")
							.delegate(
									'#deleteBtn',
									'click',
									function() {
										delChk = $("input[name='proId']:checked");

										if (delChk.length > 0) {
											if (confirm("Are you sure delete the record?")) {

												var ids = "";
												var delUrl = "delete.do?ids=";

												for (var i = 0; i < delChk.length; i++) {

													if (i == delChk.length - 1) {
														delUrl = delUrl
																+ delChk.get(i).value;
													} else {
														delUrl = delUrl
																+ delChk.get(i).value
																+ ",";
													}
												}
												alert(delUrl);
												$.ajax({
															url : delUrl,

															data : {
																ids : ids
															},
															cache : false,

															success : function(
																	html) {

																alert("Delete successfully");
																refresh();
															},
															error : function(
																	html) {

																alert("Delete failly");
																refresh();
															}
														});
											} else {
												return;
											}
										} else {
											alert("Please select the record to delete");
											return;
										}
									});

					$('#pagination2')
							.jqPaginator(
									{
										totalCounts : totalCount,
										currentPage : pageIndex,
										pageSize : pSize,
										visiblePages : 10,
										prev : '<li class="prev"><a href="javascript:;">Previous</a></li>',
										next : '<li class="next"><a href="javascript:;">Next</a></li>',
										page : '<li class="page"><a href="javascript:;">{{page}}</a></li>',
										onPageChange : function(num, type) {
											// $('#p2').text(type + '：' + num);

											var queryUrl = "queryByPage.do";
											$
													.ajax({
														url : queryUrl,
														dataType : 'json',
														data : {
															pageIndex : num,
															pageSize : pSize
														},
														cache : false,

														success : function(data) {
															$(
																	"#productList tr:not(:first)")
																	.remove();
															var results = data.pageResult.resultList;

															for (var i = 0; i < results.length; i++) {
																var rowContent = "<tr>"
																		+ "<td><input name='proId' class='checkbox' type='checkbox' value='"
																		+ results[i].pid
																		+ "'/></td>"
																		+ "<td>"
																		+ results[i].pid
																		+ "</td>"
																		+ "<td>"
																		+ results[i].product_name
																		+ "</td>"
																		+ "<td>"
																		+ results[i].note
																		+ "</td>"
																		+ "<td>"
																		+ results[i].note
																		+ "</td>"
																		+ "<td>"
																		+ results[i].note
																		+ "</td>"
																		+ "<td><button name='editBtn' objId='"
																		+ results[i].pid
																		+ "' class='btn btn-sm-block'>修改</button></td>"
																		+ "</tr>";
																$(
																		"#productList")
																		.append(
																				rowContent);
															}
														},
														error : function(html) {
															return;
														}
													});
										}
							});

					$("#tools").delegate('#addProduct','click',function(){
						alert("hello");
						addNew();
					
					});
					
					
					$("#optBtn").delegate('#saveBtn', 'click',
							function() {
								var form = $("#productInfo");
								
								form.action = "/product/save.do";
								if (vaildate(form)) {
									alert("hhhhhhhhhhhhhh");
									form.submit();

								} else {
									alert("请录入完整信息");
									return;
								}

							});

				});

function addNew() {
	$("#productDialog input").each(function(){
		  $(this).prop("value",null);
		
	});
	
	$("#productDialog textarea").each(function(){
		  $(this).prop("value",null);
		
	});
	
	$("#productDialog").modal('show');
}

function loadProduct(product) {
	alert("product.pidproduct.pidproduct.pidproduct.pid" + product.pid);
	$("#productName").val(product.productName);
	$("#pid").val(product.pid);
	$("#productNo").val(product.productNo);
	$("#price").val(product.price);
	$("#note").val(product.note);
	$("#rule").val(product.rule);
	$("#productDialog").modal('show');
}


function vaildate(form) {
	return true;
}


function refresh() {
	window.location.reload();
}
