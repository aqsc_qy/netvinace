package net.vinace.test;

import java.util.List;

/**
 * 
 * @author 御风林海
 * 树节点
 *
 */
public abstract class TreeNode<T> {

	private List<TreeNode<T>> childrens;
	
	private TreeNode<T> parent;
	
	private Boolean isroot;

	public List<TreeNode<T>> getChildrens() {
		return childrens;
	}

	public void setChildrens(List<TreeNode<T>> childrens) {
		this.childrens = childrens;
	}

	public TreeNode<T> getParent() {
		return parent;
	}

	public void setParent(TreeNode<T> parent) {
		this.parent = parent;
	}

	public Boolean getIsroot() {
		setIsroot(getParent()==null);
		return isroot;
	}

	public void setIsroot(Boolean isroot) {
		this.isroot = isroot;
	}
	
	

	
	
	

}
