package net.vinace.test;

import net.vinace.app.product.AbstractEntry;

public class Menus extends AbstractEntry {

	private Integer id;

	private String url;

	private String name;

	private String code;

	private Integer deleteTag = 0;

	private Menus parentMenun;

	public Menus getParentMenun() {
		return parentMenun;
	}

	public void setParentMenun(Menus parentMenun) {
		this.parentMenun = parentMenun;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Menus(String name, String code, Integer id) {
		this(tableName);
		this.name = name;
		this.code = code;
		this.id = id;
	}

	public Menus(String tableName) {
		// TODO Auto-generated constructor stub
		super();
		this.setTableName(tableName);
	   setPrimaryKeyField("id");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDeleteTag() {
		return deleteTag;
	}

	public void setDeleteTag(Integer deleteTag) {
		this.deleteTag = deleteTag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menus other = (Menus) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
