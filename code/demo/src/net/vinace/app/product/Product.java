package net.vinace.app.product;

import java.util.Arrays;

import javax.print.attribute.standard.Sides;


/**
 * 
 * @author Administrator 产品基本信息表
 */
public class Product extends AbstractEntry{

	

	private Integer pid;

	private String product_code;

	private String product_name;

	private byte[] detail;

	private String note;

	public Product(){
		super();
		
		setPrimaryKeyField("pid");
	}
	
	public Product(String tableName) {
		// TODO Auto-generated constructor stub
		super();
		this.setTableName(tableName);
		setPrimaryKeyField("pid");
		
		//this.setPrimaryKeyField(getClass().getDeclaredField("pid").getName());
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public byte[] getDetail() {
		return detail;
	}

	public void setDetail(byte[] detail) {
		this.detail = detail;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(detail);
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((pid == null) ? 0 : pid.hashCode());
		result = prime * result
				+ ((product_code == null) ? 0 : product_code.hashCode());
		result = prime * result
				+ ((product_name == null) ? 0 : product_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (!Arrays.equals(detail, other.detail))
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (pid == null) {
			if (other.pid != null)
				return false;
		} else if (!pid.equals(other.pid))
			return false;
		if (product_code == null) {
			if (other.product_code != null)
				return false;
		} else if (!product_code.equals(other.product_code))
			return false;
		if (product_name == null) {
			if (other.product_name != null)
				return false;
		} else if (!product_name.equals(other.product_name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [pid=" + pid + ", product_code=" + product_code
				+ ", product_name=" + product_name + ", detail="
				+ Arrays.toString(detail) + ", note=" + note + "]";
	}


	
}
