package net.vinace.app.product;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConversionException;


public class AbstractEntry {

	/**
	 * 主键字段名称
	 */
	protected String primaryKeyField;
	
	protected static  String  tableName;
	
	private Boolean isNew;

	public AbstractEntry()
	{
		
	}

	public static String getTableName() {
		return tableName;
	}

	public static void setTableName(String tableName) {
		AbstractEntry.tableName = tableName;
	}

	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	public Boolean getIsNew() {
		try {
			if(isNew==null){
			Field primaryKey=getClass().getDeclaredField(getPrimaryKeyField());
			primaryKey.setAccessible(true);
			isNew=primaryKey.get(this)==null;
			}
			return isNew;
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isNew == null) ? 0 : isNew.hashCode());
		result = prime * result
				+ ((primaryKeyField == null) ? 0 : primaryKeyField.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEntry other = (AbstractEntry) obj;
		if (isNew == null) {
			if (other.isNew != null)
				return false;
		} else if (!isNew.equals(other.isNew))
			return false;
		if (primaryKeyField == null) {
			if (other.primaryKeyField != null)
				return false;
		} else if (!primaryKeyField.equals(other.primaryKeyField))
			return false;
		return true;
	}

	/**
	 * object to map
	 * @param obj
	 * @return
	 */
	public BeanMap toMap() 	throws ConversionException{
		Object obj;
		try {
			obj = BeanUtils.cloneBean(this);
			if(obj!=null){
				BeanMap map=new BeanMap(obj);
				map=removeItem("class",map);
//				map=removeItem("primaryKeyField", map);
				return map;
				}else {
					return new BeanMap();
				}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConversionException(e.getMessage());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConversionException(e.getMessage());
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConversionException(e.getMessage());
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ConversionException(e.getMessage());
		}
		
	}

	private BeanMap removeItem(String key, BeanMap map) {
		// TODO Auto-generated method stub
		if(map.containsKey(key)){
		for(Iterator<String> iterator=map.keyIterator();iterator.hasNext();)
		{
			String k=iterator.next();
			if(key.equalsIgnoreCase(k))
			{
		           iterator.remove();
		           break;
			}
		}
		}
		return map;
		
	}

	public String getPrimaryKeyField() {
		return primaryKeyField;
	}

	public void setPrimaryKeyField(String primaryKeyField) {
		this.primaryKeyField = primaryKeyField;
	}

	@Override
	public String toString() {
		return "AbstractEntry [primaryKeyField=" + primaryKeyField + ", isNew="
				+ isNew + "]";
	}
	
	
	
	
}
