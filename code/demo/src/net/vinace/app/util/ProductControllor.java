package net.vinace.app.util;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.DocFlavor.READER;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.RequestWrapper;

import net.vinace.app.product.Product;
import net.vinace.app.product.ProductVO;

import org.apache.commons.beanutils.ConversionException;
import org.codehaus.jackson.map.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

@Controller
public class ProductControllor {

	@Autowired
	private ProductService productService;

	private int pageSize = 10;

	private int pageIndex = 1;

	private Long totalCount = new Long(0);

	private int pageCount = 0;

	private List<Product> list;

	private ProductVO vo;

	private final static String tableName = "nvproduct";

	public ProductControllor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "/product/queryByPage")
	@ResponseBody
	public Map queryByPage(
			@RequestParam(value = "pageIndex", required = true) Integer pageIndex,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,HttpServletRequest request) {
		Map resultMap = new HashMap();
		
		try {
			if (pageSize == null || "".equals(pageSize)) {
				pageSize = 30;
			}
			if (pageIndex == null || "".equals(pageIndex)) {
				pageIndex = 1;
			}
			setPageIndex(pageIndex);

			setPageSize(pageSize);

			setTotalCount(productService.totalCount(tableName, Product.class));

			List<Product> resultList = productService.queryByPage(tableName,
					Product.class, getPageIndex(), getPageSize(),
					productService.getConditionMap());
			setList(resultList);

			if (getTotalCount() % pageSize == 0) {
				setPageCount((int) (getTotalCount() / pageSize));
			} else {
				setPageCount((int) (getTotalCount() / pageSize) + 1);
			}
			PageUtils<Product> page = new PageUtils<Product>();
			page.setPageCount(getPageCount());
			page.setPageIndex(getPageIndex());
			page.setPageSize(getPageSize());
			page.setTotalCount(getTotalCount());
			page.setResultList(getList());
			resultMap.put("pageResult", page);
			return resultMap;
		} catch (SQLException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();

			return null;
		}
	}

	@RequestMapping(value = "/product/list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView view = new ModelAndView();

		try {
			setTotalCount(productService.totalCount(tableName, Product.class));

			list = productService.queryByPage(tableName, Product.class,
					getPageIndex(), getPageSize(),
					productService.getConditionMap());

			if (getTotalCount() % pageSize == 0) {
				pageCount = (int) (getTotalCount() / pageSize);
			} else {
				pageCount = (int) (getTotalCount() / pageSize) + 1;
			}
			PageUtils<Product> page = new PageUtils<Product>();
			page.setPageCount(getPageCount());
			page.setPageIndex(getPageIndex());
			page.setPageSize(getPageSize());
			page.setTotalCount(getTotalCount());
			page.setResultList(getList());
			String resultPage="product/list.jsp";
			view.setViewName(resultPage);
			view.addObject("pageResult", page);
			//request.getSession().setAttribute("pageResult", page);
			return view;
		} catch (SQLException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();

			return null;
		}

	}

	@RequestMapping(value = "/product/delete")
	public ModelAndView delete(
			@RequestParam(value = "ids", required = true) String[] ids) {
		Integer[] idArr = null;
		ModelAndView view = new ModelAndView();
		if (ids != null && ids.length > 0) {
			if (ids[0] != null && !"".equals(ids[0])) {
				if (ids[0].indexOf(",") != -1) {
					String[] idStrs = ids[0].split(",");
					idArr = new Integer[idStrs.length];
					for (int idx = 0; idx < idStrs.length; idx++) {
						idArr[idx] = Integer.parseInt(idStrs[idx]);
					}
				} else {
					idArr = new Integer[] { Integer.parseInt(ids[0]) };
				}
			}
			try {
				productService.delete(tableName, idArr, null);
				view.setViewName("product/main");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				view.setViewName("product/error");
			}
		}

		return view;

	}

	@RequestMapping(value = "/product/save", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView save(@ModelAttribute("productInfo") ProductVO vo,
			HttpServletRequest request, HttpServletResponse response) {

		System.out.println("hello world");
		ModelAndView model = new ModelAndView();
		System.out.println(vo.getProductName());
		System.out.println(vo.getProductNo());
		System.out.println(vo.getRule());
		System.out.println(vo.getNote());
		System.out.println(vo.getPrice());
		Product product = new Product(tableName);
		System.out.println("product object is new? " + vo.getIsNew());
		product.setProduct_code(vo.getProductNo());
		product.setPid(vo.getPid());
		product.setProduct_name(vo.getProductName());
		product.setNote(vo.getNote());
		product.getIsNew();
		product.setDetail(vo.getRule().getBytes());

		try {
			productService.save(tableName, product.toMap());
		} catch (ConversionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return model;

	}

	@RequestMapping(value = "/product/show")
	@ResponseBody
	public Map show(@RequestParam(value = "id", required = true) String proId,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		if (proId != null && !"".equals(proId)) {
			// Map resultMap=new HashMap<K, V>();
			System.out.println("showshowshowshowshow " + proId);
			productService.addConditon("pid", "=" + proId);
			Product product;
			try {
				product = productService.getDetail(tableName, Product.class,
						proId);
				if (product != null) {
					setVo(new ProductVO(tableName));
					getVo().setPid(product.getPid());
					getVo().setProductName(product.getProduct_name());
					getVo().setProductNo(product.getProduct_code());
					getVo().setNote(product.getNote());
					System.out.println("showshowshowshowshow " + proId);
					System.out.println("vo.note=====" + getVo().getNote());
					System.out.println("vo.pid=====" + getVo().getPid());
					getVo().setRule("just for test");
					
					getVo().setPrice("0");

					return getVo().toMap();
				} else {
					return null;
				}
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;

	}

	@RequestMapping(value = "/product/update", method = RequestMethod.POST)
	public ModelAndView update(@ModelAttribute("productInfo") ProductVO vo,
			HttpServletRequest request, HttpServletResponse response) {

		System.out.println("hello world");
		ModelAndView model = new ModelAndView();
		// System.out.println(vo.getProductName());
		// System.out.println(vo.getProductNo());
		// System.out.println(vo.getRule());
		// System.out.println(vo.getNote());
		// System.out.println(vo.getPrice());

		Product product = new Product(tableName);
		if (!vo.getIsNew()) {
			productService.addConditon(product.getPrimaryKeyField(), "="
					+ product.getPid());
			try {
				product = productService.queryFirt(tableName, Product.class,
						productService.getConditionMap());
				product.setProduct_code(vo.getProductNo());
				product.setProduct_name(vo.getProductName());
				product.setNote(vo.getNote());
				product.setDetail(vo.getRule().getBytes());
				productService.addConditon(product.getPrimaryKeyField(), "="
						+ product.getPid());
				productService.update(tableName,
						productService.getConditionMap(), product.toMap());

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return model;

	}

	public ProductVO getVo() {

		return vo;
	}

	public void setVo(ProductVO vo) {
		this.vo = vo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public List<Product> getList() {
		return list;
	}

	public void setList(List<Product> list) {
		this.list = list;
	}

}
