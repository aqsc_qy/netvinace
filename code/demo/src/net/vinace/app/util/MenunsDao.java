package net.vinace.app.util;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;
/**
 * 
 * @author root
 *
 */
@Repository
public class MenunsDao extends DBUtilsImpls<Menus>{

	@Override
	public Menus getDetail(String tableName, Class clz, Object id)
			throws SQLException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException,
			InstantiationException {

		String sql = "select * from " + tableName+" where id="+id;

		return  getRunner().query(sql,getObjectResultHandler(clz));
	}

	public List<Menus> getRootMenuList() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Menus> getMenuList(Integer parentMenunId) throws SQLException {
	String sql="select menuns.* from MENUNINFOS menuns where menuns.parentId="+parentMenunId;
		return getRunner().query(sql,getObjectResultHandler(Menus.class));
	}

}
