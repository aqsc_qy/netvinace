package net.vinace.app.util;

public class Conditon {

	private String fieldName;

	private Object fieldValue;

	private Operator operator;

	private LogicalOperator logicalConditon;

	public String genarateConditon() {

		StringBuffer condition = new StringBuffer();

		if (fieldName != null && !"".equals(fieldName) && fieldValue != null
				&& !"".equals(fieldValue)) {

			if (operator.getOperate().equalsIgnoreCase(Operator.LIKE)) {
				fieldValue = " '%" + fieldValue + "%' ";
			}
			condition.append(fieldName + operator.getOperate() + fieldValue);
			if (getLogicalConditon() != null
					&& getLogicalConditon().getOperate() != null
					&& !"".equalsIgnoreCase(getLogicalConditon().getOperate())) {
				condition.append(" " + getLogicalConditon().getOperate() + " ");
			}

		}
		System.out.println(condition.toString());
		return condition.toString();

	}

	public Conditon(String fieldName, Object fieldValue, Operator operator,
			LogicalOperator logicalConditon) {
		super();
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.operator = operator;
		this.logicalConditon = logicalConditon;
	}

	public Conditon(String fieldName, Object fieldValue, Operator operator) {
		super();
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.operator = operator;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public LogicalOperator getLogicalConditon() {
		return logicalConditon;
	}

	public void setLogicalConditon(LogicalOperator logicalConditon) {
		this.logicalConditon = logicalConditon;
	}

}
