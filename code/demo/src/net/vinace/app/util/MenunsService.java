package net.vinace.app.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenunsService extends GeneicService<Menus> implements IMenunsService {

	@Autowired
	private MenunsDao dao;

	@Override
	public List<Menus> loadAll(String tableName, Class<Menus> clz)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Menus> query(String tableName, Class<Menus> clz, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Menus queryFirt(String tableName, Class clz, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(String tableName, BeanMap parms) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(String tableName, Map conditionMap, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(String tableName, Object[] ids, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Menus> queryByPage(String tableName, Class<Menus> clz,
			int pageIndex, int pageSize, Map<String, Object> conditionMap)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long totalCount(String tableName, Class<Menus> clz)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Menus> getMenuList(Integer parentMenunId) throws SQLException {
		// TODO Auto-generated method stub
		List<Menus> menuns=new ArrayList<Menus>();
		
		if(parentMenunId==null ||parentMenunId==-1)
		{
			menuns=dao.getRootMenuList();
		}
		else{
			menuns=dao.getMenuList(parentMenunId);
		}
		return menuns;
	}
	
	

}
