package net.vinace.app.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.internet.NewsAddress;
import javax.sql.DataSource;

import net.vinace.app.product.AbstractEntry;
import net.vinace.app.product.Product;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 御风林海 DBUtils 数据库操作类 泛型DAO 抽象类
 * @since 1.0.0
 */
@Repository
public abstract class DBUtilsImpls<T> implements IGenericDAO<T> {

	/**
	 * 数据源
	 */
	@Autowired
	public DataSource dsSource;

	/**
	 * 查询助手
	 */

	protected QueryRunner runner;

	public DBUtilsImpls() {
		runner = new QueryRunner(dsSource);
	}

	@Override
	public List<T> query(String tableName, Class<T> clz, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		String sql = "select * from " + tableName;

		return getRunner().query(sql, getResultSetHandler(clz));
	}

	@Override
	public List<T> loadAll(String tableName, Class<T> clz) throws SQLException {
		// TODO Auto-generated method stub
		String sql = "select * from " + tableName;

		return getRunner().query(sql, getResultSetHandler(clz));
	}

	@Override
	public T queryFirt(String tableName, Class clz, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		StringBuffer sql = new StringBuffer("select * from " + tableName);

		if (!parms.isEmpty()) {
			StringBuffer conditionString = new StringBuffer();
			StringBuffer conditons = new StringBuffer(" where ");
			int idx = 0;
			for (Iterator<Entry<String, Object>> iterator = parms.entrySet()
					.iterator(); iterator.hasNext();) {
				Entry<String, Object> conditonEntry = iterator.next();
				String fieldString = conditonEntry.getKey();
				Object valueString = conditonEntry.getValue();
				if (fieldString == null || "".equals(fieldString)) {
					iterator.remove();
					continue;
				}
				if (idx == 0) {
					conditons.append(fieldString + " " + valueString);
				} else {
					conditons.append(" and " + fieldString + " " + valueString);
				}
				idx++;

			}
			sql.append(conditons);
		}

		List<T> list = new ArrayList<T>();
		System.out.println(sql.toString());
		list = getRunner().query(sql.toString(), getResultSetHandler(clz));
		if (!list.isEmpty()) {
			return list.get(0);
		} else {
			return null;
		}

	}

	@Override
	public int save(String tableName, BeanMap parms) throws SQLException {

		return saveOrUpdate(tableName, parms);
	}

	private int saveObject(String tableName, BeanMap parms) throws SQLException {
		StringBuffer sql = new StringBuffer("insert into " + tableName + "(");
		StringBuffer vals = new StringBuffer("values(");

		if (!parms.isEmpty()) {

			int idx = 0;
			for (Iterator<String> iterator = parms.keyIterator(); iterator
					.hasNext();) {
				String field = iterator.next();
				int fieldSize = parms.keySet().size();
				if (field == null || "".equals(field.toString())) {
					continue;
				} else if ("isNew".equals(field)
						|| "primaryKeyField".equals(field)) {
					iterator.remove();
					continue;
				}

				if (idx == (fieldSize - 1)) {
					sql.append(field);
					vals.append("?");
				} else {
					sql.append(field + ",");
					vals.append("?,");
				}
				idx++;

			}
			sql.append(")");
			vals.append(")");
			sql = sql.append(vals);
			Object[] val = (Object[]) parms.values().toArray();
			System.err.println(sql.toString());
			return getRunner().update(sql.toString(), val);
		}

		return -1;

	}

	private int saveOrUpdate(String tableName, BeanMap parms)
			throws SQLException {
		// TODO Auto-generated method stub
		if (parms.containsKey("isNew")) {
			boolean isNew = Boolean.valueOf(parms.get("isNew").toString())
					.booleanValue();
			if (!isNew) {
				return updateObject(tableName, null, parms);
			} else {
				return saveObject(tableName, parms);
			}
		}
		return -1;

	}

	private int updateObject(String tableName,
			Map<String, Object> conditionsMap, BeanMap parms)
			throws SQLException {

		if (conditionsMap == null) {
			conditionsMap = new HashMap<String, Object>();
		}
		StringBuffer sql = new StringBuffer("update " + tableName + " set ");
		String primaryKey = null;
		if (!parms.isEmpty()) {
			if (parms.containsKey("primaryKeyField")
					&& parms.get("primaryKeyField") != null) {
				primaryKey = parms.get("primaryKeyField").toString();
				conditionsMap.put(primaryKey, parms.get(primaryKey));
			}
			int idx = 0;
			for (Iterator<String> iterator = parms.keyIterator(); iterator
					.hasNext();) {
				String field = iterator.next();
				int fieldSize = parms.keySet().size();
				System.out.println(field);

				if (field == null || "".equals(field)) {
					continue;
				} else if ("isNew".equals(field)
						|| "primaryKeyField".equals(field)
						|| primaryKey.equals(field)) {

					iterator.remove();
					parms.remove(field);

					continue;
				}

				if (idx == (fieldSize - 1)) {
					sql.append(field + "=?");

				} else {
					sql.append(field + "=?,");

				}
				idx++;
			}

			System.err.println(sql.toString());

			// return 0;

		}

		if (!conditionsMap.isEmpty()) {
			StringBuffer conditons = new StringBuffer(" where ");
			int idx = 0;
			for (Iterator<Entry<String, Object>> iterator = conditionsMap
					.entrySet().iterator(); iterator.hasNext();) {
				Entry<String, Object> conditonEntry = iterator.next();
				String fieldString = conditonEntry.getKey();
				Object valueString = conditonEntry.getValue();
				if (fieldString == null || "".equals(fieldString)) {
					iterator.remove();
					continue;
				}
				if (idx == 0) {
					conditons.append(fieldString + "=" + valueString);
				} else {
					conditons.append(" and " + fieldString + "=" + valueString);
				}
				idx++;

			}
			sql.append(conditons);
		}
		System.err.println(sql.toString());
		Object[] val = (Object[]) parms.values().toArray();
		return getRunner().update(sql.toString(), val);
		// return -1;
	}

	@Override
	public int update(String tableName, Map<String, Object> conditionsMap,
			Map parms) throws SQLException {

		StringBuffer sql = new StringBuffer("update " + tableName + " ");
		StringBuffer vals = new StringBuffer(" set ");

		if (!parms.isEmpty()) {
			Object[] fields = parms.keySet().toArray();

			for (int i = 0; i < fields.length; i++) {
				System.out.println(fields[i].toString());

				if (fields[i] == null || "".equals(fields[i].toString())) {
					continue;
				}

				if (i == (fields.length - 1)) {
					sql.append(fields[i].toString() + "=?");

				} else {
					sql.append(fields[i].toString() + "=?,");

				}

			}

			System.err.println(sql.toString());
			// return getRunner().update(sql.toString(), val);
			// return 0;

		}

		if (!conditionsMap.isEmpty()) {
			StringBuffer conditons = new StringBuffer(" where ");
			int idx = 0;
			for (Iterator<Entry<String, Object>> iterator = conditionsMap
					.entrySet().iterator(); iterator.hasNext();) {
				Entry<String, Object> conditonEntry = iterator.next();
				String fieldString = conditonEntry.getKey();
				Object valueString = conditonEntry.getValue();
				if (fieldString == null || "".equals(fieldString)) {
					iterator.remove();
					continue;
				}
				if (idx == 0) {
					conditons.append(fieldString + " " + valueString);
				} else {
					conditons.append(" and " + fieldString + " " + valueString);
				}
				idx++;

			}
			sql.append(conditons);
		}
		System.err.println(sql.toString());
		Object[] val = (Object[]) parms.values().toArray();
		return getRunner().update(sql.toString(), val);
	}

	@Override
	public List<T> queryByPage(String tableName, Class<T> clz,
			int startRecordIdx, int pagSize,
			Map<String, Object> conditionsMap) throws SQLException {
		// TODO 未实现分页方法

		StringBuffer sql = new StringBuffer("select * from " + tableName);
		
		if (!conditionsMap.isEmpty()) {
			StringBuffer conditons = new StringBuffer(" where ");
			int idx = 0;
			for (Iterator<Entry<String, Object>> iterator = conditionsMap
					.entrySet().iterator(); iterator.hasNext();) {
				Entry<String, Object> conditonEntry = iterator.next();
				String fieldString = conditonEntry.getKey();
				Object valueString = conditonEntry.getValue();
				if (fieldString == null || "".equals(fieldString)) {
					iterator.remove();
					continue;
				}
				if (idx == 0) {
					conditons.append(fieldString + " " + valueString);
				} else {
					conditons.append(" and " + fieldString + " " + valueString);
				}
				idx++;

			}
			sql.append(conditons);
		}

		sql.append(" limit " + startRecordIdx + "," + pagSize);
		System.out.println(sql.toString());
		 
		return getRunner().query(sql.toString(), getResultSetHandler(clz));
	}

	@Override
	public int delete(String tableName, Object[] ids, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		String sql = "delete from " + tableName;

		if (ids != null && ids.length > 0) {
			sql = sql + " where pid in(";
			for (int i = 0; i < ids.length; i++) {
				int id = (Integer) ids[i];
				if (i == ids.length - 1) {
					sql = sql + id + ")";
				} else {
					sql = sql + id + ",";
				}
			}
		}
		return getRunner().update(sql);

	}
	


	/**
	 * 
	 * @param tableName
	 * @param clz
	 * @return 返回某个表的总记录数
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Long totalCount(String tableName, Class<T> clz) throws SQLException {

		String sql = "select count(*) totalCount from " + tableName;

		return getRunner().query(sql, new ScalarHandler("totalCount"));

	}

	/**
	 * 获取结果处理器，抽象，根据操作实体类不同而不同，需要实现
	 * 
	 * @return
	 */
	// public abstract ResultSetHandler<List<T>> getResultSetHandler();

	public ResultSetHandler<List<T>> getResultSetHandler(Class<T> clz) {
		// TODO Auto-generated method stub

		ResultSetHandler<List<T>> handler = new BeanListHandler<T>(clz);
		return handler;
	}
	
	public BeanHandler getObjectResultHandler(Class<T> clz) {
		// TODO Auto-generated method stub

		BeanHandler handler = new BeanHandler(clz);
		return handler;
	}
	
	

	

	public DataSource getDsSource() {
		return dsSource;
	}

	public void setDsSource(DataSource dsSource) {
		this.dsSource = dsSource;
	}

	public QueryRunner getRunner() {
		runner = new QueryRunner(dsSource);
		return runner;
	}

	public void setRunner(QueryRunner runner) {
		this.runner = runner;
	}

}
