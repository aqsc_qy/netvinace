package net.vinace.app.util;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class MenunsControllor extends GeneicControllor {

	private List<Menus> menusList;

	@Autowired
	private MenunsService service;
	
	private final static String tableName = "MENUNSINFOS";

	public List<Menus> getMenusList() {
		return menusList;
	}

	public void setMenusList(List<Menus> menusList) {
		this.menusList = menusList;
	}
	
	
	@RequestMapping(value = "/menun/list")
	@ResponseBody
	public Map getMenuns(@RequestParam(value = "parentMenunId") Integer parentMenunId)
	{
		Map menunList=new HashMap();
		if(parentMenunId==null)
		{
			parentMenunId=-1;
		}
		try {
			List menus=	service.getMenuList(parentMenunId);
			menunList.put("menun", menus);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return menunList;
	}
	
}
