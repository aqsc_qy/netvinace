package net.vinace.app.util;


public class Operator {

	public final  static String EQ = " = ";
	
	public final  static  String iS=" IS ";

	public final  static  String NOT = " <> ";

	public final  static  String LESS = " < ";

	public final  static  String BIGGER = " > ";

	public final  static  String LIKE = " LIKE ";

	public final  static  String LESS_THAN = " <= ";

	public final  static  String BIGGER_THAN = " >= ";
	
	public final  static  String ISNULL = " IS NULL ";
    
//	public final  static  String LOGICAL_AND=" AND ";
//	
//	public final  static  String LOGICAL_OR=" OR ";
//	
//	public final  static  String lOGICAL_NOT=" NOT ";
	
	
	private String operate;


	public Operator(String operate) {
		super();
		this.operate = operate;
	}


	public String getOperate() {
		return operate;
	}


	public void setOperate(String operate) {
		this.operate = operate;
	}
	
	
	
	
}
