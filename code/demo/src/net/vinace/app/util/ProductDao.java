package net.vinace.app.util;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.stereotype.Repository;

import net.vinace.app.product.Product;

/**
 * 
 * @author 御风林海
 * 产品信息DAO操作类
 *
 */
@Repository
public  class ProductDao extends DBUtilsImpls<Product> {

	@Override
	public Product getDetail(String tableName, Class clz, Object id)
			throws SQLException {
		
		String sql = "select * from " + tableName+" where pid="+id;

		return  getRunner().query(sql,getObjectResultHandler(clz));
	}

	
	
	
	
}
