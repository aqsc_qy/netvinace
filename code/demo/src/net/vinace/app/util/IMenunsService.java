package net.vinace.app.util;

import java.sql.SQLException;
import java.util.List;

public interface IMenunsService extends IGenericService<Menus>  {

	public List getMenuList(Integer parentMenunId) throws SQLException;
	
}
