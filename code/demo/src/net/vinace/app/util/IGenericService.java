package net.vinace.app.util;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanMap;

public interface IGenericService<T> {

	/**
	 * 
	 * @param tableName
	 *            , parms
	 * @return 查询方法，返回类型T的对象列表
	 * @throws SQLException
	 */
	public List<T> loadAll(String tableName, Class<T> clz) throws SQLException;

	/**
	 * 
	 * @return
	 * @param tableName
	 *            , type 查询方法，返回所有类型T的对象列表
	 * @throws SQLException
	 */

	public List<T> query(String tableName, Class<T> clz, Map parms)
			throws SQLException;

	/**
	 * 
	 * @param tableName
	 *            , parms
	 * @return 查询方法，返回一个类型T的对象
	 * @throws SQLException
	 */
	public T queryFirt(String tableName, Class clz, Map parms)
			throws SQLException;

	/**
	 * 
	 * @param tableName
	 *            , obj
	 * @return 保存方法，保存一个对象实体，并返回持久化后的类型T的对象
	 * @throws SQLException
	 */
	public int save(String tableName, BeanMap parms) throws SQLException;

	/**
	 * 
	 * @param tableName
	 *            , ids，parms
	 * @return 更新方法，更新一个对象实体，并返回受影响对象的数量
	 * @throws SQLException
	 */
	public int update(String tableName, Map conditionMap, Map parms)
			throws SQLException;

	/**
	 * 
	 * @param tableName
	 *            , ids，parms
	 * @return 删除方法，删除一个对象实体，并返回受影响对象的数量
	 * @throws SQLException
	 */
	public int delete(String tableName, Object[] ids, Map parms)
			throws SQLException;

	/**
	 * 
	 * @param tableName
	 * @param clz
	 * @param pageIndex
	 * @param pageSize
	 * @param conditionMap
	 * @return  分页查询方法
	 * @throws SQLException
	 */
	public List<T> queryByPage(String tableName, Class<T> clz, int pageIndex,
			int pageSize, Map<String, Object> conditionMap) throws SQLException;

	/**
	 * 
	 * @param tableName
	 * @param clz
	 * @return 返回某个表的总记录数
	 * @throws SQLException 
	 */
	public Long totalCount(String tableName, Class<T> clz) throws SQLException;
}
