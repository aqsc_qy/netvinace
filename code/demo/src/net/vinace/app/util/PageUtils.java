package net.vinace.app.util;

import java.util.List;

public class PageUtils<T> {
	
	private int pageSize = 10;

	private int pageIndex = 1;

	private Long totalCount = new Long(0);

	private int pageCount = 0;
	
	private List<T> resultList;

	public PageUtils(int pageSize, int pageIndex, Long totalCount,
			int pageCount, List<T> resultList) {
		super();
		this.pageSize = pageSize;
		this.pageIndex = pageIndex;
		this.totalCount = totalCount;
		this.pageCount = pageCount;
		this.resultList = resultList;
	}

	public PageUtils(List<T> resultList) {
		super();
		this.resultList = resultList;
	}

	public PageUtils() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public List<T> getResultList() {
		return resultList;
	}

	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}
	
	
	
}
