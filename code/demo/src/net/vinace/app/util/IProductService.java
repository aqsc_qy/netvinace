package net.vinace.app.util;

import net.vinace.app.product.Product;


public interface IProductService extends IGenericService<Product> {

}
