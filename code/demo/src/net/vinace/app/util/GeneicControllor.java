package net.vinace.app.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

import javax.persistence.Entity;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public abstract class GeneicControllor<T> extends AbstractController {

	protected String tableName;

	protected String defaultSQL;

	private static final String clzLoadErrorMsg = "类加载失败";

	public GeneicControllor()  {
		// TODO Auto-generated constructor stub
		Class enetryClz=getGeneicType(this.getClass()) ;
		String tableName= resoleTbleEntity(enetryClz);
		
	}

	protected String resoleTbleEntity(Class enetryClz) {
		if (enetryClz != null) {
			System.out.println("enetryClz=============="+enetryClz);
			TableEntity annotation = (TableEntity) enetryClz
					.getAnnotation(TableEntity.class);
			if (annotation != null && annotation.value() != null
					&& !"".equals(annotation)) {
				setTableName(annotation.value());
				System.out.println("tableName :" + getTableName()+" has been found");
				return getTableName();
			}

		} else {
			throw new NullPointerException("enetryClz is null");
		}
		return getTableName();
	}

	public Class getGeneicType(Class clz) throws NullPointerException {
		Type type = clz.getGenericSuperclass();
		if (type != null) {
			TypeVariable [] variables= type.getClass().getTypeParameters();
			if(variables!=null && variables.length>0){
			return variables[0].getClass();
			}
		} else {
			throw new NullPointerException(clzLoadErrorMsg);
		}
		return null;
	}

	protected String getDefaultSQL() {
		return defaultSQL;
	}

	protected void setDefaultSQL(String defaultSQL) {
		this.defaultSQL = defaultSQL;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		return super.handleRequest(arg0, arg1);
	}

}
