package net.vinace.app.util;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeService {

	public WelcomeService() {
		// TODO Auto-generated constructor stub
	}
	

	@RequestMapping(value="/welcome") 
	public String forwardLoginPage()
	{
		return "index.jsp";
	}

}
