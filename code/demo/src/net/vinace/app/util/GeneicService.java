package net.vinace.app.util;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class GeneicService<T> implements IGenericService<T>{

	public	Map<String,Object> conditionMap;
	
	
	public Map<String,Object>  getConditionMap() {
		
			conditionMap=new HashMap<String,Object>();
		
		return conditionMap;
	}

	public void setConditionMap(Map<String,Object> conditionMap) {
		this.conditionMap = conditionMap;
	}
	
	public int addConditon(String key,Object value) {
		getConditionMap().put(key, value);
		return getConditionMap().size();
	}
	
	public int removeConditon(String key) {
		
		if(getConditionMap().containsKey(key))
		{
			for(Iterator<String> iterator=getConditionMap().keySet().iterator();iterator.hasNext();)
			{
				String k=iterator.next();
				if(k.equals(key))
				{
					iterator.remove();
					break;
				}
			}
			
		}else{
			return -1;
		}
		return 1;
	}

	
}
