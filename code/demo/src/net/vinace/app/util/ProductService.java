package net.vinace.app.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.vinace.app.product.Product;

@Service
public class ProductService extends GeneicService<Product> implements
		IProductService {

	@Autowired
	private ProductDao dao;

	public ProductService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Product> loadAll(String tableName, Class<Product> clz)
			throws SQLException {
		// TODO Auto-generated method stub
		return dao.loadAll(tableName, clz);
	}

	@Override
	public List<Product> query(String tableName, Class<Product> clz, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		return dao.query(tableName, clz, parms);
	}

	@Override
	public Product queryFirt(String tableName, Class clz, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		return dao.queryFirt(tableName, clz, parms);
	}

	@Override
	public int save(String tableName, BeanMap parms) throws SQLException {
		// TODO Auto-generated method stub
		return dao.save(tableName, parms);
	}

	@Override
	public int update(String tableName, Map conditonMap, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub
		return dao.update(tableName, conditonMap, parms);
	}

	@Override
	public int delete(String tableName, Object[] ids, Map parms)
			throws SQLException {
		// TODO Auto-generated method stub

		return dao.delete(tableName, ids, parms);
	}

	@Override
	public List<Product> queryByPage(String tableName, Class<Product> clz,
			int pageIndex, int pageSize, Map<String, Object> conditionMap)
			throws SQLException {
		// TODO Auto-generated method stub
		int lastRecordIdx = pageIndex*pageSize;
		int startRecordIdx = 0;
		if (pageIndex > 1) {
			lastRecordIdx = pageIndex * pageSize;
			startRecordIdx = (lastRecordIdx - pageSize) + 1;
		}
//		System.out.println("startRecordIdx="+startRecordIdx);
		
		 List<Product>  results=dao.queryByPage(tableName, clz, startRecordIdx, pageSize,
				conditionMap);
		return results;
	}

	@Override
	public Long totalCount(String tableName, Class<Product> clz) throws SQLException {
		// TODO Auto-generated method stub
		return dao.totalCount(tableName, clz);
	}

	public Product getDetail(String tableName, Class<Product> clz,
			String proId) throws SecurityException, SQLException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		// TODO Auto-generated method stub
		
		return dao.getDetail(tableName, clz, proId);
	}

}
