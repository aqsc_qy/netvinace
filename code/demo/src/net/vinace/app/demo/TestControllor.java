package net.vinace.app.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import net.vinace.app.util.GeneicControllor;

@Controller
public class TestControllor {


	@RequestMapping(value = "/test/demo")
	public ModelAndView  query() {
		ModelAndView model=new ModelAndView();
	
		model.setViewName("demo/demo.html");
		return model;
	}
	

}
