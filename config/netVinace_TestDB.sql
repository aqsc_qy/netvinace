/*
SQLyog v10.2 
MySQL - 5.0.51b-community-nt-log : Database - netvinace
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`netvinace` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `netvinace`;

/*Table structure for table `ams_acsinfos` */

DROP TABLE IF EXISTS `ams_acsinfos`;

CREATE TABLE `ams_acsinfos` (
  `acsid` int(13) NOT NULL,
  `roleid` int(13) default NULL,
  `resid` int(13) default NULL,
  `retid` int(13) default NULL,
  `ctrl` varchar(100) collate utf8_bin default NULL,
  PRIMARY KEY  (`acsid`),
  KEY `FK_RoleResAccess` (`resid`),
  KEY `FK_resourceTypeInfor` (`retid`),
  KEY `FK_roleAccess` (`roleid`),
  CONSTRAINT `FK_roleAccess` FOREIGN KEY (`roleid`) REFERENCES `ams_roleinfos` (`rid`),
  CONSTRAINT `FK_resourceTypeInfor` FOREIGN KEY (`retid`) REFERENCES `ams_restypeinfos` (`retid`),
  CONSTRAINT `FK_RoleResAccess` FOREIGN KEY (`resid`) REFERENCES `ams_resinfos` (`reid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统权限信息';

/*Data for the table `ams_acsinfos` */

/*Table structure for table `ams_activedetail` */

DROP TABLE IF EXISTS `ams_activedetail`;

CREATE TABLE `ams_activedetail` (
  `serid` int(13) NOT NULL,
  `act_id` int(13) default NULL,
  `member_id` int(13) default NULL,
  `work_descp` varchar(1000) collate utf8_bin default NULL,
  `cometime` datetime default NULL,
  `gotime` datetime default NULL,
  `result` varchar(10) collate utf8_bin default NULL,
  PRIMARY KEY  (`serid`),
  KEY `FK_ActiveteInfors` (`act_id`),
  KEY `FK_memberId` (`member_id`),
  CONSTRAINT `FK_memberId` FOREIGN KEY (`member_id`) REFERENCES `ams_userinfos` (`uid`),
  CONSTRAINT `FK_ActiveteInfors` FOREIGN KEY (`act_id`) REFERENCES `ams_activeinfos` (`act_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='活动/会议详细信息，成员信息';

/*Data for the table `ams_activedetail` */

/*Table structure for table `ams_activeinfos` */

DROP TABLE IF EXISTS `ams_activeinfos`;

CREATE TABLE `ams_activeinfos` (
  `act_id` int(13) NOT NULL,
  `act_name` varchar(100) collate utf8_bin default NULL,
  `act_status` int(11) default NULL,
  `act_descp` varchar(1000) collate utf8_bin default NULL,
  `act_note` varchar(200) collate utf8_bin default NULL,
  `act_address` varchar(200) collate utf8_bin default NULL,
  `starts` datetime default NULL,
  `ends` datetime default NULL,
  `creator` int(13) default NULL,
  PRIMARY KEY  (`act_id`),
  KEY `FK_checkerId` (`creator`),
  CONSTRAINT `FK_checkerId` FOREIGN KEY (`creator`) REFERENCES `ams_userinfos` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='活动/会议基本信息表';

/*Data for the table `ams_activeinfos` */

/*Table structure for table `ams_buginfos` */

DROP TABLE IF EXISTS `ams_buginfos`;

CREATE TABLE `ams_buginfos` (
  `serid` int(13) NOT NULL,
  `tmid` int(13) default NULL,
  `pro_id` int(13) default NULL,
  `uploaderId` int(13) default NULL,
  `attach` varchar(200) collate utf8_bin default NULL,
  `commit_time` datetime default NULL,
  `last_time` datetime default NULL,
  `handelId` int(13) default NULL,
  `chksum` varchar(128) collate utf8_bin default NULL,
  `bug_note` varchar(100) collate utf8_bin default NULL,
  PRIMARY KEY  (`serid`),
  KEY `FK_HandelInfosRef` (`handelId`),
  KEY `FK_ProjectTeamInfosRef` (`tmid`),
  KEY `FK_projectInfosRef` (`pro_id`),
  KEY `FK_uploaderInfosRef` (`uploaderId`),
  CONSTRAINT `FK_uploaderInfosRef` FOREIGN KEY (`uploaderId`) REFERENCES `ams_userinfos` (`uid`),
  CONSTRAINT `FK_HandelInfosRef` FOREIGN KEY (`handelId`) REFERENCES `ams_userinfos` (`uid`),
  CONSTRAINT `FK_projectInfosRef` FOREIGN KEY (`pro_id`) REFERENCES `ams_projectinfos` (`pro_id`),
  CONSTRAINT `FK_ProjectTeamInfosRef` FOREIGN KEY (`tmid`) REFERENCES `ams_protminfos` (`tmid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目bug跟踪信息';

/*Data for the table `ams_buginfos` */

/*Table structure for table `ams_msginfos` */

DROP TABLE IF EXISTS `ams_msginfos`;

CREATE TABLE `ams_msginfos` (
  `serid` int(13) NOT NULL,
  `msg_content` varchar(200) collate utf8_bin default NULL,
  `senderId` int(13) default NULL,
  `toId` int(13) default NULL,
  `Msg_type` int(13) default NULL,
  PRIMARY KEY  (`serid`),
  KEY `FK_Msg_ToMember` (`toId`),
  KEY `FK_Msg_TypeInfor` (`Msg_type`),
  CONSTRAINT `FK_Msg_TypeInfor` FOREIGN KEY (`Msg_type`) REFERENCES `ams_msgtype` (`mtid`),
  CONSTRAINT `FK_Msg_ToMember` FOREIGN KEY (`toId`) REFERENCES `ams_userinfos` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统信息表';

/*Data for the table `ams_msginfos` */

/*Table structure for table `ams_msgtype` */

DROP TABLE IF EXISTS `ams_msgtype`;

CREATE TABLE `ams_msgtype` (
  `mtid` int(13) NOT NULL,
  `typeName` varchar(20) collate utf8_bin default NULL,
  `handelClz` varchar(100) collate utf8_bin default NULL,
  PRIMARY KEY  (`mtid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='消息类型信息表';

/*Data for the table `ams_msgtype` */

/*Table structure for table `ams_procinfos` */

DROP TABLE IF EXISTS `ams_procinfos`;

CREATE TABLE `ams_procinfos` (
  `procid` int(13) NOT NULL,
  `proid` int(13) default NULL,
  `procname` varchar(100) collate utf8_bin default NULL,
  `descp` text collate utf8_bin,
  `attach` varchar(200) collate utf8_bin default NULL,
  PRIMARY KEY  (`procid`),
  KEY `FK_projectProcess` (`proid`),
  CONSTRAINT `FK_projectProcess` FOREIGN KEY (`proid`) REFERENCES `ams_projectinfos` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目里程碑信息';

/*Data for the table `ams_procinfos` */

/*Table structure for table `ams_projectinfos` */

DROP TABLE IF EXISTS `ams_projectinfos`;

CREATE TABLE `ams_projectinfos` (
  `pro_id` int(13) NOT NULL,
  `pro_name` varchar(100) collate utf8_bin default NULL,
  `pro_status` int(11) default NULL,
  `pro_descp` varchar(1000) collate utf8_bin default NULL,
  `pro_note` varchar(200) collate utf8_bin default NULL,
  `resp_type` varchar(500) collate utf8_bin default NULL,
  `streamType` int(13) default NULL,
  PRIMARY KEY  (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目基本信息表';

/*Data for the table `ams_projectinfos` */

/*Table structure for table `ams_protmdetail` */

DROP TABLE IF EXISTS `ams_protmdetail`;

CREATE TABLE `ams_protmdetail` (
  `prm_id` int(13) NOT NULL,
  `tmid` int(13) default NULL,
  `pro_id` int(13) default NULL,
  `uid` int(13) default NULL,
  `rid` int(13) default NULL,
  `prm_note` varchar(100) collate utf8_bin default NULL,
  PRIMARY KEY  (`prm_id`),
  KEY `FK_ProjectMemberRef` (`uid`),
  KEY `FK_ProjectMemberRole` (`rid`),
  KEY `FK_ProjectTeamMembers` (`tmid`),
  CONSTRAINT `FK_ProjectTeamMembers` FOREIGN KEY (`tmid`) REFERENCES `ams_protminfos` (`tmid`),
  CONSTRAINT `FK_ProjectMemberRef` FOREIGN KEY (`uid`) REFERENCES `ams_userinfos` (`uid`),
  CONSTRAINT `FK_ProjectMemberRole` FOREIGN KEY (`rid`) REFERENCES `ams_roleinfos` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目组成员信息';

/*Data for the table `ams_protmdetail` */

/*Table structure for table `ams_protminfos` */

DROP TABLE IF EXISTS `ams_protminfos`;

CREATE TABLE `ams_protminfos` (
  `tmid` int(13) NOT NULL,
  `tmname` varchar(20) collate utf8_bin default NULL,
  `pro_id` int(13) default NULL,
  `tmnote` varchar(100) collate utf8_bin default NULL,
  `leader` int(13) default NULL,
  PRIMARY KEY  (`tmid`),
  KEY `FK_ProjectTeamLeader` (`leader`),
  KEY `FK_projectTeamInfos` (`pro_id`),
  CONSTRAINT `FK_projectTeamInfos` FOREIGN KEY (`pro_id`) REFERENCES `ams_projectinfos` (`pro_id`),
  CONSTRAINT `FK_ProjectTeamLeader` FOREIGN KEY (`leader`) REFERENCES `ams_userinfos` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目组信息';

/*Data for the table `ams_protminfos` */

/*Table structure for table `ams_resinfos` */

DROP TABLE IF EXISTS `ams_resinfos`;

CREATE TABLE `ams_resinfos` (
  `reid` int(13) NOT NULL,
  `resname` varchar(100) collate utf8_bin default NULL,
  `restype` int(13) default NULL,
  `rstatus` varchar(20) collate utf8_bin default NULL,
  `resurl` varchar(200) collate utf8_bin default NULL,
  `chksum` varchar(128) collate utf8_bin default NULL,
  `note` varchar(200) collate utf8_bin default NULL,
  PRIMARY KEY  (`reid`),
  KEY `FK_resourceType` (`restype`),
  CONSTRAINT `FK_resourceType` FOREIGN KEY (`restype`) REFERENCES `ams_restypeinfos` (`retid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统资源信息';

/*Data for the table `ams_resinfos` */

/*Table structure for table `ams_restypeinfos` */

DROP TABLE IF EXISTS `ams_restypeinfos`;

CREATE TABLE `ams_restypeinfos` (
  `retid` int(13) NOT NULL,
  `retname` varchar(100) collate utf8_bin default NULL,
  `handleClass` varchar(100) collate utf8_bin default NULL,
  `note` varchar(100) collate utf8_bin default NULL,
  PRIMARY KEY  (`retid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统资源类型信息';

/*Data for the table `ams_restypeinfos` */

/*Table structure for table `ams_roleinfos` */

DROP TABLE IF EXISTS `ams_roleinfos`;

CREATE TABLE `ams_roleinfos` (
  `rid` int(13) NOT NULL,
  `rname` varchar(20) collate utf8_bin default NULL,
  `rtype` varchar(20) collate utf8_bin default NULL,
  `rstatus` varchar(10) collate utf8_bin default NULL,
  `note` varchar(200) collate utf8_bin default NULL,
  PRIMARY KEY  (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统角色基本信息,包含系统角色和项目角色';

/*Data for the table `ams_roleinfos` */

/*Table structure for table `ams_taskinfos` */

DROP TABLE IF EXISTS `ams_taskinfos`;

CREATE TABLE `ams_taskinfos` (
  `serid` int(13) NOT NULL,
  `tmid` int(13) default NULL,
  `pro_id` int(13) default NULL,
  `senderId` int(13) default NULL,
  `receverId` int(13) default NULL,
  `attach` varchar(200) collate utf8_bin default NULL,
  `commit_Time` datetime default NULL,
  `last_time` datetime default NULL,
  `chksum` varchar(128) collate utf8_bin default NULL,
  `tskstatus` varchar(10) collate utf8_bin default NULL,
  `task_note` varchar(100) collate utf8_bin default NULL,
  PRIMARY KEY  (`serid`),
  KEY `FK_TaskReceverInfosRef` (`receverId`),
  KEY `FK_TaskSenderInfosRef` (`senderId`),
  KEY `FK_Task_ProjectInfosRef` (`pro_id`),
  KEY `FK_Task_ProjectTeamInfors` (`tmid`),
  CONSTRAINT `FK_Task_ProjectTeamInfors` FOREIGN KEY (`tmid`) REFERENCES `ams_protminfos` (`tmid`),
  CONSTRAINT `FK_TaskReceverInfosRef` FOREIGN KEY (`receverId`) REFERENCES `ams_userinfos` (`uid`),
  CONSTRAINT `FK_TaskSenderInfosRef` FOREIGN KEY (`senderId`) REFERENCES `ams_userinfos` (`uid`),
  CONSTRAINT `FK_Task_ProjectInfosRef` FOREIGN KEY (`pro_id`) REFERENCES `ams_projectinfos` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='任务信息';

/*Data for the table `ams_taskinfos` */

/*Table structure for table `ams_userinfos` */

DROP TABLE IF EXISTS `ams_userinfos`;

CREATE TABLE `ams_userinfos` (
  `uid` int(13) NOT NULL,
  `username` varchar(20) collate utf8_bin default NULL,
  `pwd` varchar(128) collate utf8_bin default NULL,
  `question` varchar(100) collate utf8_bin default NULL,
  `answer` varchar(100) collate utf8_bin default NULL,
  `ustatus` int(11) default NULL,
  `phone` varchar(13) collate utf8_bin default NULL,
  `qq` varchar(13) collate utf8_bin default NULL,
  `mobile` varchar(13) collate utf8_bin default NULL,
  `mail` varchar(50) collate utf8_bin default NULL,
  `addr` varchar(200) collate utf8_bin default NULL,
  `note` varchar(200) collate utf8_bin default NULL,
  PRIMARY KEY  (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统用户信息表';

/*Data for the table `ams_userinfos` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
