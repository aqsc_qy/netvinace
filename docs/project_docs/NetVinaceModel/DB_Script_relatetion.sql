/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2013-1-29 16:13:23                           */
/*==============================================================*/


drop table if exists Ams_acsinfos;

drop table if exists Ams_activeDetail;

drop table if exists Ams_activeinfos;

drop table if exists Ams_buginfos;

drop table if exists Ams_msgType;

drop table if exists Ams_msginfos;

drop table if exists Ams_procinfos;

drop table if exists Ams_projectinfos;

drop table if exists Ams_protmDetail;

drop table if exists Ams_protminfos;

drop table if exists Ams_resInfos;

drop table if exists Ams_resTypeInfos;

drop table if exists Ams_roleinfos;

drop table if exists Ams_taskinfos;

drop table if exists Ams_userinfos;

/*==============================================================*/
/* Table: Ams_acsinfos                                          */
/*==============================================================*/
create table Ams_acsinfos
(
   acsid                longblob not null,
   roleid               longblob,
   resid                longblob,
   retid                longblob,
   ctrl                 varchar(100)
);

alter table Ams_acsinfos comment '系统权限信息';

/*==============================================================*/
/* Table: Ams_activeDetail                                      */
/*==============================================================*/
create table Ams_activeDetail
(
   serid                longblob not null,
   act_id               longblob,
   member_id            longblob,
   work_descp           varchar(1000),
   cometime             datetime,
   gotime               datetime,
   result               varchar(10)
);

alter table Ams_activeDetail comment '活动/会议详细信息，成员信息';

/*==============================================================*/
/* Table: Ams_activeinfos                                       */
/*==============================================================*/
create table Ams_activeinfos
(
   act_id               longblob not null,
   act_name             varchar(100),
   act_status           int,
   act_descp            varchar(1000),
   act_note             varchar(200),
   act_address          varchar(200),
   starts               datetime,
   ends                 datetime,
   creator              longblob
);

alter table Ams_activeinfos comment '活动/会议基本信息表';

/*==============================================================*/
/* Table: Ams_buginfos                                          */
/*==============================================================*/
create table Ams_buginfos
(
   serid                longblob not null,
   tmid                 longblob,
   pro_id               longblob,
   uploaderId           longblob,
   attach               varchar(200),
   commit_time          datetime,
   last_time            datetime,
   handelId             longblob,
   chksum               varchar(128),
   bug_note             varchar(100)
);

alter table Ams_buginfos comment '项目bug跟踪信息';

/*==============================================================*/
/* Table: Ams_msgType                                           */
/*==============================================================*/
create table Ams_msgType
(
   mtid                 longblob not null,
   typeName             varchar(20),
   handelClz            varchar(100)
);

alter table Ams_msgType comment '消息类型信息表';

/*==============================================================*/
/* Table: Ams_msginfos                                          */
/*==============================================================*/
create table Ams_msginfos
(
   serid                longblob not null,
   msg_content          varchar(200),
   senderId             longblob,
   toId                 longblob,
   Msg_type             longblob
);

alter table Ams_msginfos comment '系统信息表';

/*==============================================================*/
/* Table: Ams_procinfos                                         */
/*==============================================================*/
create table Ams_procinfos
(
   procid               longblob not null,
   proid                longblob,
   procname             varchar(100),
   descp                text,
   attach               varchar(200)
);

alter table Ams_procinfos comment '项目里程碑信息';

/*==============================================================*/
/* Table: Ams_projectinfos                                      */
/*==============================================================*/
create table Ams_projectinfos
(
   pro_id               longblob not null,
   pro_name             varchar(100),
   pro_status           int,
   pro_descp            varchar(1000),
   pro_note             varchar(200),
   resp_type            varchar(500),
   streamType           longblob
);

alter table Ams_projectinfos comment '项目基本信息表';

/*==============================================================*/
/* Table: Ams_protmDetail                                       */
/*==============================================================*/
create table Ams_protmDetail
(
   prm_id               longblob not null,
   tmid                 longblob,
   pro_id               longblob,
   uid                  longblob,
   rid                  longblob,
   prm_note             varchar(100)
);

alter table Ams_protmDetail comment '项目组成员信息';

/*==============================================================*/
/* Table: Ams_protminfos                                        */
/*==============================================================*/
create table Ams_protminfos
(
   tmid                 longblob not null,
   tmname               varchar(20),
   pro_id               longblob,
   tmnote               varchar(100),
   leader               longblob
);

alter table Ams_protminfos comment '项目组信息';

/*==============================================================*/
/* Table: Ams_resInfos                                          */
/*==============================================================*/
create table Ams_resInfos
(
   reid                 longblob not null,
   "rename"             varchar(100),
   restype              longblob,
   status               varchar(20),
   resurl               Varchar(200),
   chksum               Varchar(128),
   note                 varchar(200)
);

alter table Ams_resInfos comment '系统资源信息';

/*==============================================================*/
/* Table: Ams_resTypeInfos                                      */
/*==============================================================*/
create table Ams_resTypeInfos
(
   retid                longblob not null,
   retname              varchar(100),
   handleClass          varchar(100),
   note                 varchar(100)
);

alter table Ams_resTypeInfos comment '系统资源类型信息';

/*==============================================================*/
/* Table: Ams_roleinfos                                         */
/*==============================================================*/
create table Ams_roleinfos
(
   rid                  longblob not null,
   rname                Varchar(20),
   rtype                varchar(20),
   status               varchar(10),
   note                 Varchar(200)
);

alter table Ams_roleinfos comment '系统角色基本信息,包含系统角色和项目角色';

/*==============================================================*/
/* Table: Ams_taskinfos                                         */
/*==============================================================*/
create table Ams_taskinfos
(
   serid                longblob not null,
   tmid                 longblob,
   pro_id               longblob,
   senderId             longblob,
   receverId            longblob,
   attach               varchar(200),
   commit_Time          datetime,
   last_time            datetime,
   chksum               varchar(128),
   status               varchar(10),
   task_note            varchar(100)
);

alter table Ams_taskinfos comment '任务信息';

/*==============================================================*/
/* Table: Ams_userinfos                                         */
/*==============================================================*/
create table Ams_userinfos
(
   uid                  longblob not null,
   username             varchar(20),
   pwd                  varchar(128),
   question             varchar(100),
   answer               varchar(100),
   status               int,
   phone                varchar(13),
   qq                   varchar(13),
   mobile               varchar(13),
   mail                 varchar(50),
   addr                 Varchar(200),
   note                 varchar(200)
);

alter table Ams_userinfos comment '系统用户信息表';

alter table Ams_acsinfos add constraint FK_RoleResAccess foreign key (resid)
      references Ams_resInfos (reid) on delete restrict on update restrict;

alter table Ams_acsinfos add constraint FK_resourceTypeInfor foreign key (retid)
      references Ams_resTypeInfos (retid) on delete restrict on update restrict;

alter table Ams_acsinfos add constraint FK_roleAccess foreign key (roleid)
      references Ams_roleinfos (rid) on delete restrict on update restrict;

alter table Ams_activeDetail add constraint FK_ActiveteInfors foreign key (act_id)
      references Ams_activeinfos (act_id) on delete restrict on update restrict;

alter table Ams_activeDetail add constraint FK_memberId foreign key (member_id)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_activeinfos add constraint FK_checkerId foreign key (creator)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_buginfos add constraint FK_HandelInfosRef foreign key (handelId)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_buginfos add constraint FK_ProjectTeamInfosRef foreign key (tmid)
      references Ams_protminfos (tmid) on delete restrict on update restrict;

alter table Ams_buginfos add constraint FK_projectInfosRef foreign key (pro_id)
      references Ams_projectinfos (pro_id) on delete restrict on update restrict;

alter table Ams_buginfos add constraint FK_uploaderInfosRef foreign key (uploaderId)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_msginfos add constraint FK_Msg_ToMember foreign key (toId)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_msginfos add constraint FK_Msg_TypeInfor foreign key (Msg_type)
      references Ams_msgType (mtid) on delete restrict on update restrict;

alter table Ams_procinfos add constraint FK_projectProcess foreign key (proid)
      references Ams_projectinfos (pro_id) on delete restrict on update restrict;

alter table Ams_protmDetail add constraint FK_ProjectInfosRef foreign key (pro_id)
      references Ams_projectinfos (pro_id) on delete restrict on update restrict;

alter table Ams_protmDetail add constraint FK_ProjectMemberRef foreign key (uid)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_protmDetail add constraint FK_ProjectMemberRole foreign key (rid)
      references Ams_roleinfos (rid) on delete restrict on update restrict;

alter table Ams_protmDetail add constraint FK_ProjectTeamMembers foreign key (tmid)
      references Ams_protminfos (tmid) on delete restrict on update restrict;

alter table Ams_protminfos add constraint FK_ProjectTeamLeader foreign key (leader)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_protminfos add constraint FK_projectTeamInfos foreign key (pro_id)
      references Ams_projectinfos (pro_id) on delete restrict on update restrict;

alter table Ams_resInfos add constraint FK_resourceType foreign key (restype)
      references Ams_resTypeInfos (retid) on delete restrict on update restrict;

alter table Ams_taskinfos add constraint FK_TaskReceverInfosRef foreign key (receverId)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_taskinfos add constraint FK_TaskSenderInfosRef foreign key (senderId)
      references Ams_userinfos (uid) on delete restrict on update restrict;

alter table Ams_taskinfos add constraint FK_Task_ProjectInfosRef foreign key (pro_id)
      references Ams_projectinfos (pro_id) on delete restrict on update restrict;

alter table Ams_taskinfos add constraint FK_Task_ProjectTeamInfors foreign key (tmid)
      references Ams_protminfos (tmid) on delete restrict on update restrict;

