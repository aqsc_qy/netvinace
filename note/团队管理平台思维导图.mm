<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node BACKGROUND_COLOR="#ffff00" COLOR="#ff6666" CREATED="1354705377359" ID="ID_561957284" MODIFIED="1355061143203">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><font size="5">&#31995;&#32479;&#37197;&#32622;&#31649;&#29702;&#26694;&#26550;</font></b>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="idea"/>
<icon BUILTIN="ksmiletris"/>
<icon BUILTIN="xmag"/>
<icon BUILTIN="full-8"/>
<hook NAME="accessories/plugins/RevisionPlugin.properties"/>
<node CREATED="1354705377359" ID="ID_181008468" MODIFIED="1355061148296" POSITION="left" STYLE="fork" TEXT="&#x56e2;&#x961f;&#x7ba1;&#x7406;&#x5e73;&#x53f0;">
<icon BUILTIN="idea"/>
<icon BUILTIN="ksmiletris"/>
<icon BUILTIN="full-1"/>
<node CREATED="1354706509203" ID="ID_1332489804" MODIFIED="1355060687984" TEXT="&#x7edf;&#x4e00;&#x7684;&#x7cfb;&#x7edf;&#x67b6;&#x6784;">
<cloud/>
<icon BUILTIN="full-0"/>
<node CREATED="1354705445468" ID="ID_1730194070" MODIFIED="1355061156890" TEXT="&#x591a;&#x79cd;&#x7528;&#x6237;&#x4ea4;&#x4e92;&#x6a21;&#x5f0f;">
<cloud/>
<node CREATED="1354705523078" ID="ID_989240550" MODIFIED="1355060687984" TEXT="GUI&#x6a21;&#x5f0f;&#xff08;C/S&#x7ed3;&#x6784;&#xff09;">
<icon BUILTIN="full-2"/>
<icon BUILTIN="full-3"/>
<node CREATED="1354706331500" ID="ID_581889393" MODIFIED="1355060687984" TEXT="&#x670d;&#x52a1;&#x7aef;&#x8bbe;&#x8ba1;">
<node CREATED="1354706345218" ID="ID_662496979" MODIFIED="1355060687984" TEXT="&#x591a;&#x7ebf;&#x7a0b;&#x7a0b;&#x5e8f;&#x8bbe;&#x8ba1;"/>
</node>
<node CREATED="1354706361890" ID="ID_1314051772" MODIFIED="1355060687984" TEXT="&#x5ba2;&#x6237;&#x7aef;&#x8bbe;&#x8ba1;">
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707573437" ID="ID_34508282" MODIFIED="1355060687984" TEXT="UI&#x8bbe;&#x8ba1;"/>
</node>
</node>
<node CREATED="1354705579703" ID="ID_732380374" MODIFIED="1355060687984" TEXT="web&#x6a21;&#x5f0f;&#xff08;B/S&#x7ed3;&#x6784;&#xff09;">
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1354706572718" FOLDED="true" ID="ID_1070272251" MODIFIED="1355060687984" TEXT="&#x516c;&#x5171;&#x53ef;&#x91cd;&#x7528;&#x7c7b;&#x5e93;">
<cloud/>
<icon BUILTIN="full-2"/>
<hook NAME="plugins/TimeManagementReminder.xml">
<Parameters REMINDUSERAT="1355061187343"/>
</hook>
<node CREATED="1354706641437" ID="ID_1079459376" MODIFIED="1354706663515" TEXT="&#x516c;&#x5171;&#x63a5;&#x53e3;&#xff0c;&#x7ec4;&#x4ef6;&#x96c6;"/>
<node CREATED="1354706672687" ID="ID_145580330" MODIFIED="1354706704578" TEXT="GUI&#x6a21;&#x5f0f;&#x5904;&#x7406;&#x5de5;&#x5177;&#x7c7b;"/>
<node CREATED="1354706717453" ID="ID_631091219" MODIFIED="1354706728656" TEXT="web&#x6a21;&#x5f0f;&#x5904;&#x7406;&#x5de5;&#x5177;&#x7c7b;"/>
</node>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354706962718" ID="ID_819392894" MODIFIED="1355060687984" TEXT="&#x7edf;&#x4e00;&#x7684;&#x7cfb;&#x7edf;&#x7ba1;&#x7406;&#x5e73;&#x53f0;">
<cloud/>
<icon BUILTIN="full-5"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354706994828" ID="ID_710603092" MODIFIED="1355060687984" TEXT="&#x4eba;&#x5458;&#x7ba1;&#x7406;"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707013421" ID="ID_336870862" MODIFIED="1355060687984" TEXT="&#x7cfb;&#x7edf;&#x6743;&#x9650;&#x7ba1;&#x7406;"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707027093" ID="ID_1801497321" MODIFIED="1355060687984" TEXT="&#x9879;&#x76ee;&#x6d41;&#x7a0b;&#x7ba1;&#x7406;"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707076203" ID="ID_1340152924" MODIFIED="1355060687984" TEXT="&#x7cfb;&#x7edf;&#x914d;&#x7f6e;&#x7ba1;&#x7406;"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354980745031" ID="ID_1785750088" MODIFIED="1355060687984" TEXT="&#x7cfb;&#x7edf;&#x63d2;&#x4ef6;&#x7ba1;&#x7406;"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354705655437" FOLDED="true" ID="ID_607238154" MODIFIED="1355060687984" TEXT="&#x4e0e;&#x4e3b;&#x6d41;IDE&#x96c6;&#x6210;">
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-7"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707331593" ID="ID_1651468758" MODIFIED="1354707340218" TEXT="NetBean&#x63d2;&#x4ef6;&#x5f00;&#x53d1; "/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707367343" ID="ID_47025699" MODIFIED="1354708576156" TEXT="Eclipse&#x63d2;&#x4ef6;&#x5f00;&#x53d1; ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node CREATED="1354706229250" FOLDED="true" ID="ID_1965140779" MODIFIED="1355060687984" TEXT="&#x591a;&#x5c42;&#x7ea7;&#x65e5;&#x5fd7;&#x7cfb;&#x7edf;">
<cloud/>
<icon BUILTIN="full-1"/>
<node CREATED="1354706267015" ID="ID_1154833499" MODIFIED="1354883646359" TEXT="&#x670d;&#x52a1;&#x5668;&#x7ea7;&#x65e5;&#x5fd7;">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1354706294703" ID="ID_739013493" MODIFIED="1354706308109" TEXT="&#x7cfb;&#x7edf;&#x65e5;&#x5fd7;"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707232484" ID="ID_296527292" MODIFIED="1354707262265" TEXT="AOP&#x4e0e;&#x4e8b;&#x4ef6;&#x89e6;&#x53d1;&#x7684;&#x65e5;&#x5fd7;&#x751f;&#x6210;&#x673a;&#x5236;"/>
</node>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354707486171" ID="ID_1758630691" MODIFIED="1355060687984" TEXT="SVN,CVS&#x7248;&#x672c;&#x670d;&#x52a1;&#x5668;&#x63d2;&#x4ef6;&#x96c6;&#x6210;">
<icon BUILTIN="full-6"/>
</node>
<node BACKGROUND_COLOR="#ffff00" CREATED="1354883808578" ID="ID_617915245" MODIFIED="1355060687984" TEXT="&#x5f00;&#x6e90;&#x8f6f;&#x4ef6;&#x7ba1;&#x7406;&#x5e93;">
<icon BUILTIN="full-9"/>
<icon BUILTIN="idea"/>
<icon BUILTIN="help"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffff00" CREATED="1355060940328" ID="ID_1677632386" MODIFIED="1355061023218" POSITION="right" TEXT="A&#x7cfb;&#x7edf;">
<icon BUILTIN="idea"/>
</node>
<node BACKGROUND_COLOR="#ffff00" CREATED="1355061029468" ID="ID_1977496755" MODIFIED="1355061038593" POSITION="right" TEXT="B&#x7cfb;&#x7edf;">
<icon BUILTIN="ksmiletris"/>
</node>
</node>
</map>
