/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileserver;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class TaskProcesstor extends Thread {

    private LinkedList<WorkTask> taskList;

    public TaskProcesstor() {
        taskList = new LinkedList<WorkTask>();
        
    }

    public synchronized boolean addTask(final WorkTask task) {

        taskList.push(task);
        System.out.println("new task is comed");
        notify();
        return true;
    }

    public synchronized boolean handleTask() {

        if(taskList.size()!=0){
        for(final WorkTask task:taskList){
            Thread t=new Thread(task);
            t.start();
            while(!t.isAlive()){
            taskList.remove(task);
            }
        }
        }
        try {
              System.out.println("waitting task");
            wait();
          
        } catch (InterruptedException ex) {
            Logger.getLogger(TaskProcesstor.class.getName()).log(Level.SEVERE, null, ex);
        }


        return false;
}
    public void run(){
        while(!this.isInterrupted()){
           handleTask();
        }
       
    }

}
