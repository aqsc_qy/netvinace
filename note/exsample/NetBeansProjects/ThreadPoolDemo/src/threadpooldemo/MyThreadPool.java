/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threadpooldemo;

import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class MyThreadPool {

    private static Queue<WorkTask> queue ;
    private static WorkTask exeTask;

    private MyThreadPool() {
        queue = new LinkedList<WorkTask>();
    }

    public static MyThreadPool newInstanel() {


        MyThreadPool pool = new MyThreadPool();
        if (pool == null) {
            System.err.println("Thead pool init failly....");
        }
        return pool;

    }

//    public static void main(String[] args) {
//        System.out.println("creat the task list to work...");
//        int tsize = addThread();
//        System.out.println(tsize);
//        try {
//            System.out.println("be start work...");
//            Thread.currentThread().sleep(2000);
//            doWork();
//            System.out.println(queue.size());
//
//        } catch (InterruptedException ex) {
//            Logger.getLogger(MyThreadPool.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    public  synchronized  int addTask(WorkTask task) {

        if(queue==null){
            queue = new LinkedList<WorkTask>();
        }
           queue.add(task);
          // queue.notify();
          
        return queue.size();

    }


    public synchronized int doWork(){

        for(WorkTask task:queue){
            task.start();
           
        }


        

        return 0;
    }

  private class WorkTask extends Thread{


    private String name;
   public  WorkTask(String name) {
       this.name=name;

    }
@Override
    public void run(){
        try {
            //work code

            System.out.println(this.getName() + " is working");
            Thread.currentThread().sleep(2000);
            System.out.println(this.getName() + " work finish");
        } catch (InterruptedException ex) {
            Logger.getLogger(WorkTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the name
     */

}
}
