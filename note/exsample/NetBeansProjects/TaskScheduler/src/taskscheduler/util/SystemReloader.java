/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskscheduler.util;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class SystemReloader implements SystemReloadListener {

    public boolean callVBFile(){

          RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
        String processID = runtime.getName().substring(0, runtime.getName().indexOf("@"));
        try {
            File vbsFile = new File("./lib/killWORD.vbs");
            if (vbsFile.exists()) {
                System.out.println(vbsFile.getAbsolutePath());
                Process p = Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL \""+ vbsFile.getAbsolutePath() + "\"" + " "+processID);//v1.27b
                p.waitFor();
              Thread.currentThread().sleep(5000);
             processID = runtime.getName().substring(0, runtime.getName().indexOf("@"));
             System.out.println(processID);
            } else {
                System.err.println("The tool is lost...");
                System.exit(-1);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(SystemReloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SystemReloader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;


    }


    public  boolean reload(ReloadEvent re) {
       
            System.out.println("ReloadEvent has been listener.....");
             try {
            Thread.currentThread().sleep(3000);
//           Process p=Runtime.getRuntime().exec("java -jar  ./TaskScheduler.jar");
//           InputStreamReader ins=new InputStreamReader(p.getInputStream());
//           final BufferedReader buf=new BufferedReader(ins);
           long threadID=Thread.currentThread().getId();
         Thread.currentThread().setDaemon(true);


           new Thread(){
                
               public void run()
               {
                    try {
                        //                String line="";
                        //                    try {
                        //                        while ((line = buf.readLine()) != null) {
                        //                            System.out.println(line);
                        //                        }
                        //                    } catch (IOException ex) {
                        //                        Logger.getLogger(ReloadEvent.class.getName()).log(Level.SEVERE, null, ex);
                        //                    }
                        Process p = Runtime.getRuntime().exec("java -jar  ./TaskScheduler.jar");
                          p.waitFor();

                    } catch (InterruptedException ex) {
                        Logger.getLogger(SystemReloader.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(SystemReloader.class.getName()).log(Level.SEVERE, null, ex);
                    }
               }

           }.start();
       
        } catch (InterruptedException ex) {
            Logger.getLogger(SystemReloader.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
            re.message();
            return true;
       
    }

}
