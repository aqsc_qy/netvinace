/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taskscheduler.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class SystemParameterLoader {

    public static boolean initSystem() {

        String fileName = "config.properties";
        boolean passed = false;
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                fileName = "./lib/config.properties";
                file = new File(fileName);
            }
            fileName = file.getAbsolutePath();
            FileInputStream ins = new FileInputStream(file);
            Properties props = new Properties();
            props.load(ins);
            //load the system Intilization Parameters
            passed = true;
        } catch (IOException ex) {
            passed = false;
            Logger.getLogger(SystemParameterLoader.class.getName()).log(Level.SEVERE, null, ex);
        }




        return passed;
    }
}
