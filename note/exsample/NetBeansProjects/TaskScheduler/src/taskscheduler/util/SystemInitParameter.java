/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskscheduler.util;

/**
 *
 * @author user
 */
public class SystemInitParameter {

    private static String firstStarTime;

    private static String StarTime;

    private static String stopTime;

    /**
     * @return the firstStarTime
     */
    public static String getFirstStarTime() {
        return firstStarTime;
    }

    /**
     * @param aFirstStarTime the firstStarTime to set
     */
    public static void setFirstStarTime(String aFirstStarTime) {
        firstStarTime = aFirstStarTime;
    }

    /**
     * @return the StarTime
     */
    public static String getStarTime() {
        return StarTime;
    }

    /**
     * @param aStarTime the StarTime to set
     */
    public static void setStarTime(String aStarTime) {
        StarTime = aStarTime;
    }

    /**
     * @return the stopTime
     */
    public static String getStopTime() {
        return stopTime;
    }

    /**
     * @param aStopTime the stopTime to set
     */
    public static void setStopTime(String aStopTime) {
        stopTime = aStopTime;
    }
}
