/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskscheduler.util;

import java.util.EventListener;

/**
 *
 * @author user
 */
public interface SystemReloadListener extends EventListener {

    public  boolean reload(ReloadEvent re);

    
}
