/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskscheduler.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EventObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ReloadEvent extends EventObject {

    Object obj;

    public ReloadEvent(Object obj){
        super(obj);
        this.obj=obj;
    }

    @Override
    public Object getSource(){

        return this.obj;
    }

    public void message(){
       
        System.out.println("Event has be called.....");
    }
}
