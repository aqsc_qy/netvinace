<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {font-size: 12px}
.STYLE3 {font-size: 12px; font-weight: bold; }
.STYLE4 {
	color: #03515d;
	font-size: 12px;
}
-->
</style>

<script>
var  highlightcolor='#c1ebff';
//此处clickcolor只能用win系统颜色代码才能成功,如果用#xxxxxx的代码就不行,还没搞清楚为什么:(
var  clickcolor='#51b2f6';
function  changeto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=highlightcolor&&source.id!="nc"&&cs[1].style.backgroundColor!=clickcolor)
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=highlightcolor;
}
}

function  changeback(){
if  (event.fromElement.contains(event.toElement)||source.contains(event.toElement)||source.id=="nc")
return
if  (event.toElement!=source&&cs[1].style.backgroundColor!=clickcolor)
//source.style.backgroundColor=originalcolor
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}

function  clickto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=clickcolor&&source.id!="nc")
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=clickcolor;
}
else
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}
//更新用户信息表单提交函数
function updateUser()
{
     var frm=document.getElementById("userInfor");
     var pass=chkFrm();
    
     if(pass==true){
    	 frm.action="userCURD!updateUser.action";
    	 frm.submit();
	}
	else{
	   	 return;
	}
}	
//表单验证函数
function chkFrm()
{
  var userId=document.getElementById("userId").value;
  var userName=document.getElementById("userName").value;
  var userPhone1=document.getElementById("userPhone1").value;
  //var userMobile1=document.getElementById("userMobile1").value;
  var userAddress1=document.getElementById("userAddress1").value;
   
  if(userId==""){
      alert("用户编号不能为空");
      return false;
  }
  if(userName=="")
  {
   alert("用户名称不能为空");
      return false;
  }
  if(userPhone1==""){
      alert("联系电话1不能为空");
      return false;
  }
   if(userAddress1==""){
      alert("联系地址1不能为空");
      return false;
  }
  return true
}


</script>

</head>

<body leftmargin="0" rightmargin="0" bottommargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30" background="<%=path%>/securty/images/tab_05.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" height="30"><img src="<%=path%>/securty/images/tab_03.gif" width="12" height="30" /></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="46%" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5%"><div align="center"><img src="<%=path%>/securty/images/tb.gif" width="16" height="16" /></div></td>
                <td width="95%" class="STYLE1"><span class="STYLE3">你当前的位置</span>：[系统管理]-[用户管理]</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="12"><img src="<%=path%>/securty/images/tab_07.gif" width="12" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  
   <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td background="<%=path%>/securty/images/bg04.gif"  style="background-repeat:repeat-y" width="12">e</td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr><td align="center">
          <form  name="userInfor"  method="post" id="userInfor">
          <table width="600" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="97" align="right"><span style="font-size:12px;">用户编号：</span></td>
    <td width="497" align="left"><input name="userId"  readonly id="userId"  value="<s:property value="userId"/>"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" type="text" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">用户名称：</span></td>
    <td  align="left"><input name="userName"  id="userName" type="text" value="<s:property value="userName"/>"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">性别：</span></td>
    <td align="left"><select name="userSex" id="userSex"  style="width:100px">
    <s:if test='userSex=="M"'>
        <option value="M" selected>男</option>
        <option value="F">女</option>
      </s:if>
      <s:if test='userSex=="F"'>
       <option value="F"  selected>女</option>
       <option value="M">男</option>
       </s:if>
      
    </select></td>
  </tr>
   <tr>
    <td align="right"><span style="font-size:12px">所属部门：</span></td>
    <td align="left">
    <select name="userDept" id="userDept"  style="width:170px">
    <option value="<s:property value="userDept" />" selected><s:property value="userDeptName" /></option>
     <s:iterator value="#request.deptList">
    <option value="<s:property value="deptid" />"><s:property value="deptname" /></option>
    </s:iterator>
    </select></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">联系电话1：</span></td>
    <td  align="left"><input name="userPhone1" id="userPhone1" value="<s:property value="userPhone1" default="暂无数据" />" type="text" style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">联系电话2：</span></td>
    <td  align="left"><input name="userPhone2" id="userPhone2" type="text"  value="<s:property value="userPhone2" default="暂无数据" />"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
    <tr>
    <td align="right"><span style="font-size:12px">联系手机1：</span></td>
    <td  align="left"><input name="userMobile1" id="userMobile1"  value="<s:property value="userMobile1" default="暂无数据" />"  type="text" style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
     <tr>
    <td align="right"><span style="font-size:12px">联系手机2：</span></td>
    <td  align="left"><input name="userMobile2" id="userMobile2" type="text" value="<s:property value="userMobile2"  default="暂无数据"/>" style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">联系地址1：</span></td>
    <td  align="left"><input name="userAddress1" id="userAddress1" value="<s:property value="userAddress1" default="暂无数据" />"  size="50" type="text"  style="border-style:solid; border-width:1px; border-color:#CCCCCC"/></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">联系地址2：</span></td>
    <td  align="left"> <input name="userAddress2" id="userAddress2" size="50"  type="text" value="<s:property value="userAddress2" default="暂无数据" />"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">QQ：</span></td>
    <td  align="left"><input name="userQQ"  id="userQQ" type="text" value="<s:property value="userQQ" default="暂无数据"/>"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">MSN：</span></td>
    <td  align="left"><input name="userMSN" id="userMSN" type="text" value="<s:property value="userMSN" default="暂无数据" />"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">E-Mail：</span></td>
    <td align="left"><input name="userMail" id="userMail" type="text" value="<s:property value="userMail" default="暂无数据" />"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">备注：</span></td>
    <td  align="left">
    <textarea name="userNote" id="userNote" style="border-style:solid; border-width:1px; border-color:#CCCCCC" cols="50" rows="3">
     <s:property value="userNote" default="暂无数据" />
    </textarea></td>
  </tr>
  <tr>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%">&nbsp;</td>
        <td width="21%" align="center"><input type="button" onclick="updateUser();" value="更新" /></td>
        <td width="19%" align="center"><input type="button" onclick="resetFrm();" value="重置" /></td>
        <td width="31%">&nbsp;</td>
      </tr>
    </table></td>
    </tr>
</table>
</form>
</table>
</body>
</html>
