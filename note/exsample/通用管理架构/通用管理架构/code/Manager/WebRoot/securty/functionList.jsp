<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.amos.entry.Function;"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'functionList.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!-- xtree 组件引入 -->
		<script type='text/javascript' src="js/xtree/xtree.js"></script>
		<link type="text/css" rel="stylesheet" href="js/xtree/xtree.css">
		<!-- DWR 组件引入 -->
		<script type='text/javascript' src='dwr/interface/dwrAjax.js'></script>
		<script type='text/javascript' src='dwr/engine.js'></script>
		<script type='text/javascript' src='dwr/util.js'></script>

	</head>

	<body style="margin-left: 20px">

		<div style="margin-left: 0px">
			<script type="text/javascript">
			  dwr.engine.setAsync(false);   //设置DWR同步，使得dwr回调函数与后台数据同步
   var menunList=null;            
   var mName=new Array();    //菜单名称数组
   var mId=new Array();         //菜单ID数组
   var mUri=new Array();       //菜单URI数组
   var i=0;                            //全局计数器
 var tree = new WebFXTree('系统菜单');  //构建菜单根目录 采用Xtree组件自带函数
 tree.setBehavior('explore');         //设置xtree样式风格   
   
  //初始化树  从后台获取数据，直接返回页面，由JS负责构建,检测工作 
 <% 
 List<Function> sysMenun=(List<Function> )request.getAttribute("sysMenun");
 if(sysMenun!=null){
 
 for(Function menun:sysMenun){%>
 
    mName[mName.length]='<%=menun.getFname()%>';
    mId[mId.length]='<%=menun.getFuid()%>';
    mUri[mUri.length]='<%=menun.getFuri()%>';
     dwrAjax.isHasItem(mId[i], function dealResults(result){
        
        showSubItems(mId[i],mName[i],result,true);   //显示第一级树节点
        i++;
     });
     
     <%}%>
document.write(tree);   //打印初始化后的树
//出错时转跳页面
<%}else{
        request.getRequestDispatcher("common/500.jsp").forward(request,response);
     }%>
     
 //获取子节点函数
 function showSubItems(fuId,fName,hasItems,first,itemNum)
 {
 
 if(first==true){
  var root = new WebFXTreeItem(fName); 
           root.icon=webFXTreeConfig.flordIcon;
        root.openIcon=webFXTreeConfig.openFlordIcon;
       root.action="javascript:showSubItems("+fuId+",'"+fName+"',"+hasItems+","+"false,0);javascript:functionDetail("+fuId+");";
     tree.add(root);
     return;
  }
   if(hasItems==true){
     
        var items=tree.getSelected().childNodes;
    if(items.length==0){ 
     dwrAjax.showItems(fuId,function(result){
       menunList=result;
       
     for(n=0;n<menunList.length;n++){
       dwr.engine.setAsync(false); 
       i=n;
    //ajax子节点检测函数，检测当前节点是否拥有子节点
    dwrAjax.isHasItem(menunList[i].fuid, function dealResults(hasItem){
 
  var item = new WebFXTreeItem(menunList[i].fname); 
     //拥有下一级节点，以目录方式显示
    if(hasItem==true){
     item.icon=webFXTreeConfig.flordIcon;
     item.openIcon=webFXTreeConfig.openFlordIcon;
     item.action="javascript:showSubItems("+menunList[i].fuid+",'"+menunList[i].fname+"',"+hasItem+","+"false);javascript:functionDetail("+menunList[i].fuid+");";       
}
//没有下一级节点，以文件方式显示

  else{
          item.icon=webFXTreeConfig.fileIcon;
      item.openIcon=webFXTreeConfig.fileIcon;
     item.action="javascript:functionDetail("+menunList[i].fuid+");";      
  
        }
      tree.getSelected().add(item);   
          });
         
   dwr.engine.setAsync(true); 
 }
      });
      }
      else{
      tree.getSelected().toggle();
      }
  }
      }
  //获取菜单详细信息，进入菜单信息管理页面
 function functionDetail(fuid){
 
       window.self.parent.document.frames("mainPage").location="functionCURD!getFunctionDetail.action?fuId="+fuid+"";
      
     }
     dwr.engine.setAsync(true);   //取消DWR同步
      </script>
		</div>
	</body>
</html>


