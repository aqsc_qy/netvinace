package com.amos.service.itf;

import java.util.List;

import com.amos.entry.Function;

public interface IDwrService {

	public List<Function> showItems(String parentID);
	
	public boolean isHasItem(String parentID);
	
	public Function getFunctionDetail(String funcID) ;
}
