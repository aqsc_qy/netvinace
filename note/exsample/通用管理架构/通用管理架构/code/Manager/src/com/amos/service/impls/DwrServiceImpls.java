package com.amos.service.impls;

import java.util.List;

import com.amos.dao.CommonDao;
import com.amos.entry.Function;
import com.amos.service.itf.IDwrService;
import com.amos.service.itf.IFunctionService;

public class DwrServiceImpls implements IDwrService {

	private IFunctionService functionService;

	public List<Function> showItems(String parentID) {
		
	
			List<Function>  itemList=functionService.ListFunctions(parentID);
			for(Function item:itemList){
				System.out.println(item.getFname());
			}
			
			return itemList;
		
		
		
	}

	public IFunctionService getFunctionService() {
		return functionService;
	}

	public void setFunctionService(IFunctionService functionService) {
		this.functionService = functionService;
	}

	public boolean isHasItem(String parentID) {
		
		int sign=functionService.isHasItem(parentID);
		
		System.out.println("------"+sign);
		if(sign==0){
			return false;
			}
			else{
				
				return true;
			}
	}
	
	public Function getFunctionDetail(String funcID) {
		Function function=(Function) functionService.getFunctionDetail(funcID);
		return function;
	}

}
