package com.amos.service.impls;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.transaction.interceptor.TransactionProxyFactoryBean;

import com.amos.dao.CommonDao;
import com.amos.entry.Function;
import com.amos.entry.SysUsers;
import com.amos.service.itf.ILoginService;
import com.amos.utils.IEntry;
import com.amos.utils.LoginUser;
import com.amos.utils.MD5Tool;
import com.amos.utils.SysMessage;

public class LoginServiceImpls   implements ILoginService {

	private CommonDao dao; 
	
	private SysMessage sysMessage;
	
	public SysMessage getSysMessage() {
		return sysMessage;
	}



	public void setSysMessage(SysMessage sysMessage) {
		this.sysMessage = sysMessage;
	}
	
	public CommonDao getDao() {
		return dao;
	}

	public void setDao(CommonDao dao) {
		this.dao = dao;
	}

	public String catchUserLoginDate(Date date) {
		SimpleDateFormat mf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss E ");
		String dateStr=mf.format(date);
		
		return dateStr;
	}
	
	public LoginUser login(String username, String password) {
		String securityPassword=MD5Tool.MD5(password);
		String condition=" join a.departmentses dept join dept.sysRoles roles  join roles.functionList function  where a.uname='"+username+"' and a.password.password='"+securityPassword+"' and a.statu=1 and function.fstatu=1";
	     List<IEntry> userList=dao.queryDataByCondition(condition,SysUsers.class,true);
	     String msgCode="";
	     LoginUser loginUser=new LoginUser();
	     
	     if(userList!=null&&userList.size()==1){
	    	 
	    	 SysUsers user=(SysUsers) userList.get(0);
	    	 
	    	 loginUser.setUser(user);
	    	 msgCode="LOGIN_SUCCESS";
	    	 System.out.println("======================"+user.getUrole().size());
	    	 loginUser.setLoginStatu(msgCode);
	   }else{
	    	 msgCode="LOGIN_FAIL"; 
	    	 loginUser.setLoginStatu(msgCode);
	     }
	     return  loginUser;
	     
	}



	public String getSystemMessage(String msgCode) {
		String message=sysMessage.getSystemMessage(msgCode);
		return message;
	}

}
