package com.amos.web;

import com.amos.service.itf.IDeptService;
import com.amos.service.itf.IUserService;
import com.amos.utils.PageUtil;
import com.amos.vo.SysUsersVO;
import com.opensymphony.xwork2.ActionSupport;
import com.amos.entry.Departments;
import com.amos.entry.SysRole;
import com.amos.entry.SysUsers;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

public class UserAction extends ActionSupport {

	private IUserService userService;

	private IDeptService deptService;

	private List<SysUsers> usersList;

	// User properties

	private String userId;
	private String userName;
	private String userSex;

	private String userDept;  //部门编号
	private String userDeptName;  //部门名称
	private String userPhone1;
	private String userPhone2;
	private String fmobile;
	private String smobile;
	private String userAddress1;
	private String userAddress2;
	private String userQQ;
	private String userMSN;
	private String userMail;
	private int statu = 1;
	private String userNote;
	private String password="8888";
	private String userMobile1;
	private String userMobile2;

	
	public String newUser()throws Exception{
		
		HttpServletRequest request=ServletActionContext.getRequest();
		List<Departments> deptList=deptService.listDept();
		request.setAttribute("deptList", deptList);
		
		return "new";
	}
	
	public String updateUser()throws Exception{
		HttpServletResponse response=ServletActionContext.getResponse();
		SysUsersVO userVo=new SysUsersVO( userId,  userName,  userSex,  userDept,
				userPhone1,  userPhone2,  userMobile1,  userMobile2,
				userAddress1,  userAddress2,  userQQ,  userMSN,
				userMail,  statu,  userNote, password);
		boolean sign=userService.updateUser(userVo,userId);
		PrintWriter out=response.getWriter();
		if(sign==true){
			out.println("<script>alert('更新成功');window.location='userCURD!queryAllUsers.action';</script>");
			
		}
		else{
			out.println("<script>alert('更新失败');window.location='userCURD!queryAllUsers.action';</script>");
			//out.println("<script>alert('更新失败');</script>");
		
		}
		return null;
	}
	
	public String delUsers()throws Exception{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpServletResponse response=ServletActionContext.getResponse();
		String userIds=request.getParameter("userId").toString();
		String [] userIdArr=new String[PageUtil.pageSize];
		System.out.println("userIds--------------------"+userIds);
		if(userIds.indexOf(",")!=-1){
			userIdArr=userIds.split(",");
		}
		else{
			userIdArr[0]=userIds;
		}
		int sign=userService.delUsers(userIdArr);
		PrintWriter out=response.getWriter();
		if(sign>=1){
			out.println("<script>alert('删除成功');window.location='userCURD!queryAllUsers.action';</script>");
			
		}
		else{
			out.println("<script>alert('删除失败');window.location='userCURD!queryAllUsers.action';</script>");
			//out.println("<script>alert('更新失败');</script>");
		
		}
		return null;
		
	}
	
	public String delUser()throws Exception{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpServletResponse response=ServletActionContext.getResponse();
		String userId=request.getParameter("userId").toString();
		boolean sign=userService.delUser(userId);
		PrintWriter out=response.getWriter();
		if(sign==true){
			out.println("<script>alert('删除成功');window.location='userCURD!queryAllUsers.action';</script>");
			
		}
		else{
			out.println("<script>alert('删除失败');window.location='userCURD!queryAllUsers.action';</script>");
			//out.println("<script>alert('更新失败');</script>");
		
		}
		return null;
//		if(sign==true){
//			out.println("<script>alert('删除成功');</script>");
//			return "del";
//		}
//		else{
//			out.println("<script>alert('删除失败');</script>"); 
//			return "unDel";
//		}
	}
	
	public String addUser() throws Exception {
		
		HttpServletResponse response=ServletActionContext.getResponse();
		
//		SysUsersVO userVo=new SysUsersVO(userId, userName,userSex, userDept,
//				userPhone1, userMobile1, userAddress1,password ,statu);
		
		SysUsersVO userVo=new SysUsersVO( userId,  userName,  userSex,  userDept,
				userPhone1,  userPhone2,  userMobile1,  userMobile2,
				userAddress1,  userAddress2,  userQQ,  userMSN,
				userMail,  statu,  userNote, password);
		
		boolean sign=userService.addUser(userVo);
		PrintWriter out=response.getWriter();
		if(sign==true){
			out.println("<script>alert('添加成功');window.location='userCURD!queryAllUsers.action';</script>");
			
		}
		else{
			out.println("<script>alert('添加失败');window.location='userCURD!queryAllUsers.action';</script>");
			
		
		}
		return null;
		
	}

	public String queryDeptUsers() throws Exception {

		String deptID = ServletActionContext.getRequest()
				.getParameter("deptID");
		return "list";
	}

	
	
	public String queryAllUsers() throws Exception {
		
		HttpServletRequest request=ServletActionContext.getRequest();
		

		int pageIndex=request.getParameter("pageIndex")==null?1:Integer.parseInt(request.getParameter("pageIndex").trim().toString());
		int pageSize=request.getParameter("pageSize")==null?PageUtil.pageSize:Integer.parseInt(request.getParameter("pageSize").trim().toString());
		
			
			if(pageIndex==0||pageIndex<1){
				pageIndex=1;
			}
		PageUtil currentPage=new PageUtil();

	currentPage=userService.listUserPage(pageIndex, pageSize);

		request.setAttribute("currentPage", currentPage);
		return "list";
	}
	
	
	
	public String getUsersDetail() throws Exception {
		HttpServletRequest request=ServletActionContext.getRequest();
		String userID =request.getParameter("userID");

		SysUsers user = userService.getUser(userID);
		List<Departments> deptList=deptService.listDept();
		for(Departments dept:deptList){
		    if(dept.getDeptid().equals(user.getDepartmentses().getDeptid())){
		    	deptList.remove(deptList.indexOf(dept));
		    	break;
		    }
		    else{
		    	continue;
		    }
		}
	if (user != null) {
			this.userId = user.getUid();
			this.userName = user.getUname();
			this.userPhone1 = user.getFphone();
			this.userMobile1 = user.getFmobile();
			this.userAddress1 = user.getFaddress();
			this.userAddress2 = user.getSaddress();
			this.userPhone2 = user.getSphone();
			this.smobile = user.getSmobile();
			this.userDeptName = user.getDepartmentses().getDeptname();
			this.userDept=user.getDepartmentses().getDeptid();
			this.userSex = user.getUsex();
			this.userNote = user.getNote();
			this.userMSN = user.getMsn();
			this.userQQ = user.getQq();
			this.userMail = user.getEmail();
			request.setAttribute("deptList", deptList);
			return "detail";
		} else {
			return "unDetail";
		}
	
	
	}

	// Getter/Setter Methods
	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public IDeptService getDeptService() {
		return deptService;
	}

	public void setDeptService(IDeptService deptService) {
		this.deptService = deptService;
	}

	public List<SysUsers> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<SysUsers> usersList) {
		this.usersList = usersList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserDept() {
		return userDept;
	}

	public void setUserDept(String userDept) {
		this.userDept = userDept;
	}

	public String getUserPhone1() {
		return userPhone1;
	}

	public void setUserPhone1(String userPhone1) {
		this.userPhone1 = userPhone1;
	}

	public String getUserPhone2() {
		return userPhone2;
	}

	public void setUserPhone2(String userPhone2) {
		this.userPhone2 = userPhone2;
	}

	public String getFmobile() {
		return fmobile;
	}

	public void setFmobile(String fmobile) {
		this.fmobile = fmobile;
	}

	public String getSmobile() {
		return smobile;
	}

	public void setSmobile(String smobile) {
		this.smobile = smobile;
	}

	public String getUserAddress1() {
		return userAddress1;
	}

	public void setUserAddress1(String userAddress1) {
		this.userAddress1 = userAddress1;
	}

	public String getUserAddress2() {
		return userAddress2;
	}

	public void setUserAddress2(String userAddress2) {
		this.userAddress2 = userAddress2;
	}

	public String getUserQQ() {
		return userQQ;
	}

	public void setUserQQ(String userQQ) {
		this.userQQ = userQQ;
	}

	public String getUserMSN() {
		return userMSN;
	}

	public void setUserMSN(String userMSN) {
		this.userMSN = userMSN;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public int getStatu() {
		return statu;
	}

	public void setStatu(int statu) {
		this.statu = statu;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserMobile1() {
		return userMobile1;
	}

	public void setUserMobile1(String userMobile1) {
		this.userMobile1 = userMobile1;
	}

	public String getUserMobile2() {
		return userMobile2;
	}

	public void setUserMobile2(String userMobile2) {
		this.userMobile2 = userMobile2;
	}

	public String getUserDeptName() {
		return userDeptName;
	}

	public void setUserDeptName(String userDeptName) {
		this.userDeptName = userDeptName;
	}

	


	

	


}
