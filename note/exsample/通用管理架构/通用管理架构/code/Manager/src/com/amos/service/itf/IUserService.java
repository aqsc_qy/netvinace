package com.amos.service.itf;

import java.util.List;

import com.amos.entry.SysUsers;
import com.amos.entry.SysUsers;
import com.amos.utils.PageUtil;
import com.amos.vo.SysUsersVO;

public interface IUserService {


	
	public List<SysUsers> listUser();
	
	public PageUtil listUserPage(int pageNo,int pageSize);

	public boolean delUser(String userId);
	
	public boolean addUser(SysUsersVO userVO);

	public SysUsers getUser(String userID);

	public boolean updateUser(SysUsersVO userVo ,String userId);

	public int delUsers(String[] userIdArr);
	
	
}
