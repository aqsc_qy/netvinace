package com.amos.utils;

import java.io.Serializable;
/*
 * 创建人：伍尚彬
 * 创建日期：2010-08-21
 * 用途：公共实体接口，系统所有实体对象继承该接口，获得对象序列化功能
 * 修改日期：2010-08-21
 * 版本：1.0.0
 */
public interface IEntry extends Serializable {

}
