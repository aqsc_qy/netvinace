package com.amos.entry;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.amos.utils.IEntry;

/**
 * Functionlists entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class Function  implements IEntry {

	// Fields

	private String fuid;
	private Function parent;
	private Integer id;
	private String fname;
	private Integer fstatu;
	private String furi;
	private String accstr;
	private String note;

	private Set subListses = new HashSet(0);
	private Set roleList = new HashSet(0);

	// Constructors

	/** default constructor */
	public Function() {
	}

	
	// Property accessors

	public String getFuid() {
		return this.fuid;
	}

	public void setFuid(String fuid) {
		this.fuid = fuid;
	}

	

	public Function getParent() {
		return parent;
	}


	public void setParent(Function parent) {
		this.parent = parent;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFname() {
		return this.fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}



	public Integer getFstatu() {
		return fstatu;
	}


	public void setFstatu(Integer fstatu) {
		this.fstatu = fstatu;
	}


	public String getFuri() {
		return this.furi;
	}

	public void setFuri(String furi) {
		this.furi = furi;
	}

	public String getAccstr() {
		return this.accstr;
	}

	public void setAccstr(String accstr) {
		this.accstr = accstr;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	


	public Set getSubListses() {
		this.subListses=getCurrectFunction();
		return this.subListses;
	}


	public void setSubListses(Set subListses) {
		this.subListses = subListses;
	}


	public Set getRoleList() {
		return roleList;
	}


	public void setRoleList(Set roleList) {
		this.roleList = roleList;
	}
	
	
private Set<Function> getCurrectFunction(){
		
		Iterator<Function> itFunc=this.subListses.iterator();
		while(itFunc.hasNext()){
		
			if(itFunc.next().getFstatu()==0){
				itFunc.remove();
				continue;
			}
		}
		
			return this.subListses;
	}





}