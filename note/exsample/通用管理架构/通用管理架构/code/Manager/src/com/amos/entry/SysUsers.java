package com.amos.entry;

import java.util.HashSet;
import java.util.Set;
import com.amos.utils.IEntry;

/**
 * SysUsers entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class SysUsers  implements IEntry {

	// Fields

	private String uid;
//	private Integer id;
	private String uname;
	private String usex;
	private String fphone;
	private String sphone;
	private String fmobile;
	private String smobile;
	private String faddress;
	private String saddress;
	private String qq;
	private String msn;
	private String email;
	private Integer statu;
	private String note;
	
	private Set<SysRole> urole = new HashSet(0);
	private Departments departmentses;
	private UserPassword password;

	// Constructors

	public UserPassword getPassword() {
		return password;
	}

	public void setPassword(UserPassword password) {
		this.password = password;
	}

	/** default constructor */
	public SysUsers() {
	}

	


	// Property accessors

	public String getUid() {
		return this.uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}



	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUsex() {
		return this.usex;
	}

	public void setUsex(String usex) {
		this.usex = usex;
	}

	

	public String getFphone() {
		return this.fphone;
	}

	public void setFphone(String fphone) {
		this.fphone = fphone;
	}

	public String getSphone() {
		return this.sphone;
	}

	public void setSphone(String sphone) {
		this.sphone = sphone;
	}

	public String getFmobile() {
		return this.fmobile;
	}

	public void setFmobile(String fmobile) {
		this.fmobile = fmobile;
	}

	public String getSmobile() {
		return this.smobile;
	}

	public void setSmobile(String smobile) {
		this.smobile = smobile;
	}

	public String getFaddress() {
		return this.faddress;
	}

	public void setFaddress(String faddress) {
		this.faddress = faddress;
	}

	public String getSaddress() {
		return this.saddress;
	}

	public void setSaddress(String saddress) {
		this.saddress = saddress;
	}

	public String getQq() {
		return this.qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getMsn() {
		return this.msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getStatu() {
		return this.statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Set<SysRole> getUrole() {
		return urole;
	}

	public void setUrole(Set<SysRole> urole) {
		this.urole = urole;
	}

	public Departments getDepartmentses() {
		return departmentses;
	}

	public void setDepartmentses(Departments departmentses) {
		this.departmentses = departmentses;
	}


}