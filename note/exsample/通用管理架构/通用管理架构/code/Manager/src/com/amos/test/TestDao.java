package com.amos.test;

import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.amos.utils.IEntry;


/*
 * 
 *作者：伍尚彬
 *描述: 通用DAO类,统一数据操作接口
 *版本:测试版
 *创建日期:2010-08-21
 * 
 */


public class TestDao extends HibernateDaoSupport {

	/*
	 *描述:用于保存数据
	 *参数:数据实体
	 *返回值:void
	 */
	
	public void saveData(IEntry entry){
		this.getHibernateTemplate().save(entry);
	}
	
	/*
	 *描述:用于更新数据
	 *参数:数据实体
	 *返回值:void
	 */
	
	public void updateData(IEntry entry){
		this.getHibernateTemplate().update(entry);
	}
	
	/*
	 *描述:用于删除数据
	 *参数:数据实体
	 *返回值:void
	 */
	
	public void deleteData(Class entryClass,String id){
		IEntry result=(IEntry) this.getHibernateTemplate().get(entryClass, id);
		this.getHibernateTemplate().delete(result);
		}
	
	/*
	 *描述:用于查询所有数据
	 *参数:数据实体
	 *返回值:数据实体集合List
	 */
	
	public List<IEntry> queryAllData(Class entryClass){
		List<IEntry> resultList=this.getHibernateTemplate().loadAll(entryClass);
		return resultList;
	}
	
	/*
	 *描述:用于查找单条数据
	 *参数:数据实体，主键编号
	 *返回值:实体引用
	 */
	
	public IEntry querySingleData(Class entryClass,String id){
		IEntry result=(IEntry) this.getHibernateTemplate().get(entryClass, id);
		return result;
		
	}
	
	/*
	 *描述:用于条件查询数据
	 *参数:查询条件
	 *返回值:数据实体集合List
	 */
	
	public List<IEntry> queryDataByCondition(String condition,Class entryClass){
		String HQL="from "+entryClass.getName()+" a " +condition;
		List<IEntry> resultList=this.getHibernateTemplate().find(HQL);
		return resultList;
	}
	
	
	/*
	 *描述:用于传统SQL查询数据
	 *参数:SQL语句
	 *返回值:List
	 */
	
	public List queryDataBySQL( final String sql){
			
		List resultList=(List)this.getHibernateTemplate().execute(new HibernateCallback(){
			
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				   List resultList=session.createSQLQuery(sql).list();
				return resultList;
			}
			
		});
		return resultList;
	}

	/*
	 *描述:用于传统SQL数据操作（包括：增加，修改，删除）
	 *参数:SQL语句
	 *返回值:void
	 */
	
	public void updateDataBySQL( final String sql){
		
		this.getHibernateTemplate().execute(new HibernateCallback(){
			
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				   int resultSize=session.createSQLQuery(sql).executeUpdate();
				   return resultSize;
			}
			
		});
		
	}
}
