package com.amos.service.itf;

import java.util.List;

import com.amos.entry.SysRole;
import com.amos.utils.PageUtil;
import com.amos.vo.SysRoleVO;

public interface IRoleService {

	public List<SysRole> listRole();
	
	public PageUtil listRolePage(int pageNo,int pageSize);

	public boolean delRole(String roleId);
	
	public boolean addRole(SysRoleVO roleVO);

	public SysRole getRole(String roleID);

	public boolean updateRole(SysRoleVO roleVo ,String roleId);

	public int delRoles(String[] roleIdArr);
}
