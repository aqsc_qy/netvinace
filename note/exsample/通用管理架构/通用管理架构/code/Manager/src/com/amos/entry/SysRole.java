package com.amos.entry;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.amos.utils.IEntry;


/**
 * SysRole entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class SysRole  implements IEntry {

	// Fields

	private String rid;
	private Departments departments;
	private Integer id;
	private String rname;
	private Integer statu;
	private String note;
	private Set<Function> functionList=new HashSet(0);
	private Set<SysUsers> userList=new HashSet(0);

	// Constructors

	private Set<Function> getCurrectFunction(){
		
		Iterator<Function> itFunc=functionList.iterator();
		while(itFunc.hasNext()){
		
			if(itFunc.next().getFstatu()==0){
				itFunc.remove();
				continue;
			}
		}
		
			return this.functionList;
	}
	
	
	public Set<Function> getFunctionList() {
		
		this.functionList=getCurrectFunction();
		
		return this.functionList;
	}

	public void setFunctionList(Set<Function> functionList) {
		this.functionList = functionList;
	}

	public Set<SysUsers> getUserList() {
		return userList;
	}

	public void setUserList(Set<SysUsers> userList) {
		this.userList = userList;
	}

	/** default constructor */
	public SysRole() {
	}


	// Property accessors

	public String getRid() {
		return this.rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public Departments getDepartments() {
		return this.departments;
	}

	public void setDepartments(Departments departments) {
		this.departments = departments;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRname() {
		return this.rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public Integer getStatu() {
		return this.statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}



}