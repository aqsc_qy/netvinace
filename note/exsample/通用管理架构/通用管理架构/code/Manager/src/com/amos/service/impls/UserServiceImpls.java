package com.amos.service.impls;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.interceptor.TransactionProxyFactoryBean;

import com.amos.dao.CommonDao;
import com.amos.entry.Departments;
import com.amos.entry.SysUsers;
import com.amos.entry.UserPassword;
import com.amos.service.itf.IDeptService;
import com.amos.service.itf.IUserService;
import com.amos.utils.IEntry;
import com.amos.utils.MD5Tool;
import com.amos.utils.PageUtil;
import com.amos.vo.SysUsersVO;
/**
 * 
 * @author Administrator
 * @category 用户信息管理业务层组件实现
 * @version 1.1
 * @since 1.0
 * 
 */
public class UserServiceImpls   implements IUserService {

	
	private CommonDao dao; 
	private List<SysUsers> userList=new ArrayList();
	private IDeptService deptService;
	
	/**
	 *@category 查询系统用户列表非分页方法����
	 * @return List<SysUsers> 
	 * @param null
	 * @exception null
	 * @since 1.0.0   2011-01-15
	 */
	public List<SysUsers> listUser() {
		
		List<SysUsers> userList=new ArrayList<SysUsers>();
		List<IEntry> resultList=dao.queryAllData(SysUsers.class);
		for(IEntry result:resultList){
			SysUsers user=(SysUsers) result;
			userList.add(user);
		}
		return userList;
	}
	/**
	 *@category 查询系统用户列表分页方法����
	 * @return PageUtil
	 * @param pageNo,pageSize
	 * @exception null
	 * @since 1.0.0   2011-01-15
	 */
	public PageUtil listUserPage(int pageNo, int pageSize) {
		PageUtil currentPage=dao.queryAllData(SysUsers.class, pageNo, pageSize);
		return currentPage;
	}

	/**
	 * @author Administrator
	 * @category 删除一条系统用户信息
	 * @param userId
	 * @return boolean
	 * @exception null
	 * @since 1.0.0   2011-01-17
	 */
	public boolean delUser(String userId) {
		int sign=dao.deleteData(SysUsers.class, userId);
		if(sign!=1){
			return false;
		}
		else{
			return true;
		}
	
	}
	
	/**
	 * @author Administrator
	 * @category 增加一条系统用户信息
	 * @param userVO
	 * @return boolean
	 * @exception null
	 * @since 1.0.0   2011-01-18
	 */
	public boolean addUser(SysUsersVO userVO) {
		System.out.println("userVO.getDeptId()=============="+userVO.getDeptId());
		Departments dept=deptService.getDept(userVO.getDeptId());
		
		SysUsers user=new SysUsers();
		user.setUid(userVO.getUid());
		user.setUname(userVO.getUname());
		user.setUsex(userVO.getUsex());
		user.setFphone(userVO.getFphone());
		user.setFmobile(userVO.getFmobile());
		user.setFaddress(userVO.getFaddress());
		user.setDepartmentses(dept);
		user.setStatu(userVO.getStatu());
		UserPassword password=new UserPassword(user,MD5Tool.MD5(userVO.getPassword()));
		password.setUser(user);
		user.setPassword(password);
		
		user.setSphone(userVO.getSphone());
		user.setSaddress(userVO.getSaddress());
		user.setSmobile(userVO.getSmobile());
		user.setEmail(userVO.getEmail());
		user.setQq(userVO.getQq());
		user.setMsn(userVO.getMsn());
		user.setNote(userVO.getNote());
		int sign=dao.saveData(user);
		if(sign==1){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * @author Administrator
	 * @category 获取某一条用户信息的明细
	 * @param userID
	 * @return SysUsers
	 * @exception null
	 * @since 1.0.0   2011-01-18
	 */
	
	public SysUsers getUser(String userID) {
		
		return (SysUsers) dao.querySingleData(SysUsers.class, userID);
	}
	
	
	/**
	 * @author Administrator
	 * @category 更新某一条用户信息的明细
	 * @param userVo
	 * @return boolean
	 * @exception null
	 * @since 1.0.0   2011-01-18
	 */
	
	public boolean updateUser(SysUsersVO userVO,String userId) {
		SysUsers user=(SysUsers) dao.querySingleData(SysUsers.class, userId);
		Departments dept=deptService.getDept(userVO.getDeptId());
		if(dept!=null){
		user.setDepartmentses(dept);
		user.setUname(userVO.getUname());
		user.setUsex(userVO.getUsex());
		user.setFphone(userVO.getFphone());
		user.setFmobile(userVO.getFmobile());
		user.setFaddress(userVO.getFaddress());
		user.setStatu(userVO.getStatu());
		user.setSphone(userVO.getSphone());
		user.setSaddress(userVO.getSaddress());
		user.setSmobile(userVO.getSmobile());
		user.setEmail(userVO.getEmail());
		user.setQq(userVO.getQq());
		user.setMsn(userVO.getMsn());
		user.setNote(userVO.getNote());
		int sign=dao.updateData(user);
		if(sign==1)
		{
			return true;
		}
		else{
			return false;
		}
		}
		else{
			return false;
		}
	}
	
	// Getter/Setter Methods
	
	public CommonDao getDao() {
		return dao;
	}
	public void setDao(CommonDao dao) {
		this.dao = dao;
	}
	public List<SysUsers> getUserList() {
		return userList;
	}
	public void setUserList(List<SysUsers> userList) {
		this.userList = userList;
	}
	
	
	public IDeptService getDeptService() {
		return deptService;
	}
	public void setDeptService(IDeptService deptService) {
		this.deptService = deptService;
	}
	
	
	/**
	 * @author Administrator
	 * @category 批量删除用户信息
	 * @param userIdArr
	 * @return int
	 * @exception null
	 * @since 1.0.0   2011-01-19
	 */
	
	public int delUsers(String[] userIdArr) {
		int sign=dao.deleteDatas(SysUsers.class, userIdArr);
		System.out.println("影响行数：-----------------------------------"+sign+"");
		return sign;
	}

	

}
