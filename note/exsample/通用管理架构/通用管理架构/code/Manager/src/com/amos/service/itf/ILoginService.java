package com.amos.service.itf;

import java.util.Date;

import com.amos.utils.LoginUser;

public interface ILoginService {

	public LoginUser login(String username,String password);

	public String catchUserLoginDate(Date date);

	public String getSystemMessage(String msgCode);
}
