package com.amos.service.itf;


import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amos.entry.Function;
import com.amos.vo.FunctionVO;

public interface IFunctionService {

	public List<Function> ListFunctions(String funId);
	
	public int isHasItem(String parentID);

	public Function getFunctionDetail(String parentID);

	public List<Function> listAllFunctions();

	public boolean updateFunction(String functionId,FunctionVO functionVo); 
	
	
}

