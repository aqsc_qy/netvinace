package com.amos.workflow.model;

import java.util.Date;

public class Notice implements INotice{
	
	private long streamID;
	private Date sendDate;
	private Date readDate;
	private String text;
	private String DataPackURL;
	private String note;
	private Sender sender;
	private WorkStatu statu;
	private Recvtor recvtor;
	
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public Date getReadDate() {
		return readDate;
	}
	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getDataPackURL() {
		return DataPackURL;
	}
	public void setDataPackURL(String dataPackURL) {
		DataPackURL = dataPackURL;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Sender getSender() {
		return sender;
	}
	public void setSender(Sender sender) {
		this.sender = sender;
	}
	public Recvtor getRecvtor() {
		return recvtor;
	}
	public void setRecvtor(Recvtor recvtor) {
		this.recvtor = recvtor;
	}
	public long getStreamID() {
		return streamID;
	}
	public void setStreamID(long streamID) {
		this.streamID = streamID;
	}
	public WorkStatu getStatu() {
		return statu;
	}
	public void setStatu(WorkStatu statu) {
		this.statu = statu;
	}
}
